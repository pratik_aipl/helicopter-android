import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Tools } from 'src/app/tools';
import { CommonService } from 'src/app/shared/common.service';
@Component({
  selector: 'app-mybookings',
  templateUrl: './mybookings.page.html',
  styleUrls: ['./mybookings.page.scss'],
})
export class MybookingsPage implements OnInit {
  bookingList = [];
  lat:any;
  lng:any;
  constructor(public router: Router, public tools: Tools, public commonService: CommonService) {
  }
  ngOnInit() {
  
  }
  ionViewDidEnter() {
    this.getBookedPass();
  }
  getBookedPass() {
    this.tools.openLoader();
    this.commonService.bookedPass().subscribe(data => {
      this.tools.closeLoader();
      this.tools.closeLoader();
      let res: any = data;
      console.log('Data => bookingList ', res);
      this.bookingList = res.data;
    }, (error: Response) => {
      console.log('bookedPass ==> ',error)
      console.log(error);
      this.tools.closeLoader();
      let err:any = error;
      this.tools.openAlertToken(err.status, err.error.message);      
    });
  }

  bookClick(item) {
    this.commonService.setSelPass(item);
    if(item.PassType=="Luggage"){
      this.router.navigateByUrl('/confirm-bookings-luggage/' + item.PassID );
    }else{
      this.router.navigateByUrl('/confirm-bookings');
    }
  }

}
