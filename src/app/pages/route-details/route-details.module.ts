import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouteDetailsPage } from './route-details.page';
import { SharedModule } from 'src/app/shared/shared.module';
// import { BaiduMapModule } from 'angular2-baidu-map';

const routes: Routes = [
  {
    path: '',
    component: RouteDetailsPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    // BaiduMapModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RouteDetailsPage]
})
export class RouteDetailsPageModule {}
