import { Component, OnInit, OnDestroy, AfterContentInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';

import { Platform, ToastController, LoadingController } from '@ionic/angular';

declare var google;

@Component({
  selector: 'app-route-details',
  templateUrl: './route-details.page.html',
  styleUrls: ['./route-details.page.scss'],
})

export class RouteDetailsPage implements OnInit, OnDestroy, AfterContentInit {
  route;
  selRoute;
  routeId;
  // options: MapOptions;
  status = '';
  isChina = false;
  loading: any;

  mapG: any;
  mapOptions: any;
  location = { lat: 22.3039, lng: 70.8022 };
  markerOptions: any = { position: null, map: null, title: null };
  marker: any;
  points = [];
  waypoints = [];

  @ViewChild('mapElement') mapNativeElement: ElementRef;
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;
  origin: any = {
    lat: 0,
    lng: 0
  }
  destination: any = {
    lat: 0,
    lng: 0
  }
  selMode = 'DRIVING'
  // selMode = 'TRANSIT'
  constructor(public router: Router, public commonServices: CommonService,
    public loadingCtrl: LoadingController, public toastCtrl: ToastController,
    public activatedRoute: ActivatedRoute, public plt: Platform) {
    this.selRoute = commonServices.getSelectedCountry();
    this.isChina = this.selRoute.CountryName == 'China' ? true : false;
    this.route = commonServices.getSelectedRoute();
    // console.log(' this.selRoute ', this.selRoute);
    // console.log('this.route ', this.route);

    this.points = [];
    for (let i = 0; i < this.route.routes_details.length; i++) {
      const element = this.route.routes_details[i];
      if (element.Lat != null && element.Long != null && element.Lat != '' && element.Lat != '') {
        console.log(element.Lat + ' - ' + element.Long);

        this.waypoints.push(
          {
            location:{lat: parseFloat(element.Lat),lng:parseFloat(element.Long) },
            stopover: true
         }  
        );
        this.points.push({
          title: element.PlaceName,
          lat: element.Lat,
          lng: element.Long
        });
        // add a marker
      }
    }

    this.origin.lat = parseFloat(this.points[0].lat)
    this.origin.lng = parseFloat(this.points[0].lng)

    this.destination.lat = parseFloat(this.points[this.points.length - 1].lat)
    this.destination.lng = parseFloat(this.points[this.points.length - 1].lng)
     console.log(' this.points --> ', this.points);
     this.plt.ready().then(() => {
    this.initMapJs();
     });
  }
  initMapJs() {
    let minZoomLevel = 12;
    this.mapG = new google.maps.Map(this.mapNativeElement.nativeElement, {
      zoom: minZoomLevel,
      panControl: true,
      zoomControl: true,
      mapTypeControl: true,
      scaleControl: true,
      streetViewControl: true,
      overviewMapControl: true,
      rotateControl: true,
      myLocationButton: true,
      disableDefaultUI: true,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      center: this.destination
    });
    this.directionsDisplay.setMap(this.mapG);

    //  this.drawRoute();
     this.calculateAndDisplayRoute();
  }
  drawRoute() {
    
    let pointsL = [];
    for (let i = 0; i < this.points.length; i++) {
      const element = this.points[i];
        pointsL.push({
          lat:  parseFloat(element.lat),
          lng:  parseFloat(element.lng)
        });

      const marker = new google.maps.Marker({
        position: {lat: parseFloat(this.points[i].lat), lng: parseFloat(this.points[i].lng) },
        map: this.mapG,
        title: this.points[i].title,
        snippet:this.points[i].lat+","+this.points[i].lng
        // icon: this.vehicleIcon
      });
      this.addInfoWindow(marker,  this.points[i].title);
    }

    const flightPath = new google.maps.Polyline({
      path: pointsL,
      geodesic: true,
      strokeColor: "#008000",
      strokeOpacity: 1.0,
      strokeWeight: 2,
    });

    // flightPath.setMap(this.mapG);

  }



  ngOnInit() {
    // Since ngOnInit() is executed before `deviceready` event,
    // you have to wait the event.
    // this.plt.ready().then(() => {

    //   let minZoomLevel = 14;
    //   this.mapG = new google.maps.Map(this.mapNativeElement.nativeElement, {
    //     zoom: minZoomLevel,
    //     panControl: true,
    //     zoomControl: true,
    //     mapTypeControl: true,
    //     scaleControl: true,
    //     streetViewControl: true,
    //     overviewMapControl: true,
    //     rotateControl: true,
    //     myLocationButton: true,
    //     disableDefaultUI: true,
    //     mapTypeId: google.maps.MapTypeId.ROADMAP,
    //     center: this.destination

    //   });
    //   this.directionsDisplay.setMap(this.mapG);
    //   this.calculateAndDisplayRoute();
    // });

    // let pointsL = [];
    // for (let i = 0; i < this.points.length; i++) {
    //   const element = this.points[i];
    //     pointsL.push({
    //       lat:  parseFloat(element.lat),
    //       lng:  parseFloat(element.lng)
    //     });
    //   // let marker: Marker = this.mapG .addMarkerSync({
    //   //   title: this.points[i].title,
    //   //   position:  new LatLng(this.points[i].lat, this.points[i].lng),
    //   // });

    //   const marker = new google.maps.Marker({
    //     position: {lat: parseFloat(this.points[i].lat), lng: parseFloat(this.points[i].lng) },
    //     map: this.mapG,
    //     title: this.points[i].title,
    //     // icon: this.vehicleIcon
    //   });
    //   this.addInfoWindow(marker,  this.points[i].title);
    // }

    // const flightPath = new google.maps.Polyline({
    //   path: pointsL,
    //   geodesic: true,
    //   strokeColor: "#008000",
    //   strokeOpacity: 1.0,
    //   strokeWeight: 2,
    // });

    // flightPath.setMap(this.mapG);

    // this.mapG.addPolyline({
    //   points: pointsL,
    //   'color': '#008000',
    //   'width': 5,
    //   'geodesic': true
    // });

  }
  calculateAndDisplayRoute() {

    console.log('selMode ', this.selMode);
    this.directionsService.route({
      origin: this.origin,
      destination: this.destination,
         waypoints:this.waypoints,
      //   waypoints:[{
      //     location:{lat: -34.597353,lng: -58.415832},
      //     stopover: true
      // },
      // {
      //     location:{lat: -34.608441,lng: -58.406194},
      //     stopover: true
      //  }],
      travelMode: this.selMode,
      optimizeWaypoints: true,
      // provideRouteAlternatives: true,
      // unitSystem: google.maps.UnitSystem.METRIC
    }, (response, status) => {
      if (status === 'OK') {
        // this.response =response;
        console.log('distance --> ', response.routes[response.routes.length - 1].legs[0].distance.text);
        console.log('response ', response);
        this.directionsDisplay.setDirections(response);
      } else {
        this.showToast('Directions request failed');
        console.log('Directions request failed due to ' + status);
        // window.alert('Directions request failed due to ' + status);
      }
    });
  }
  addInfoWindow(marker, content){

    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.mapG, marker);
    });
  }
  openMap() {
    this.router.navigate(['/map-route']);
  }
  async showToast(message: string) {
    let toast = await this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'middle'
    });

    toast.present();
  }
  ngAfterContentInit(): void {
    console.log('Country Name --> ', this.isChina)
  }
  _click(e: any) {
    this.status = `${e.point.lng}, ${e.point.lat}, ${+new Date()}`;
    console.log(' ==> ', this.status)
  }

  ngOnDestroy(): void {
  }
  bookPass() {
    this.router.navigateByUrl('/book-pass');
    console.log('Book Pass Click');
  }

}


// import { Component, OnInit, OnDestroy, AfterContentInit, ElementRef, ViewChild } from '@angular/core';
// import { Router, ActivatedRoute } from '@angular/router';
// import { CommonService } from 'src/app/shared/common.service';

// import { Platform, ToastController, LoadingController } from '@ionic/angular';

// declare var google;


// @Component({
//   selector: 'app-route-details',
//   templateUrl: './route-details.page.html',
//   styleUrls: ['./route-details.page.scss'],
// })

// export class RouteDetailsPage implements OnInit, OnDestroy, AfterContentInit {
//   route;
//   selRoute;
//   routeId;
//   // options: MapOptions;
//   status = '';
//   isChina = false;

//   @ViewChild('map') mapElement: ElementRef;
//   map: any;

//   // directionsService = new google.maps.DirectionsService;
//   // directionsDisplay = new google.maps.DirectionsRenderer;



//   // loading: any;

//   mapOptions: any;
//   location = { lat: 22.3039, lng: 70.8022 };
//   markerOptions: any = { position: null, map: null, title: null };
//   marker: any;
//   apiKey: any = 'AIzaSyBTE0jBqSRc14mtpM_0MDzNjvbAApxjN6c'; /*Your API Key*/
//   points=[];


//   constructor(public router: Router, public commonServices: CommonService, 
//     public loadingCtrl: LoadingController, public toastCtrl: ToastController, 
//     public activatedRoute: ActivatedRoute, public plt: Platform) {
//     this.selRoute = commonServices.getSelectedCountry();
//     this.isChina = this.selRoute.CountryName == 'China' ? true : false;
//     this.route = commonServices.getSelectedRoute();
//     console.log(' this.selRoute ', this.selRoute);
//     console.log('this.route ', this.route);    
//   }


