import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';

@Component({
  selector: 'app-route-list',
  templateUrl: './route-list.page.html',
  styleUrls: ['./route-list.page.scss'],
})
export class RouteListPage implements OnInit {

  route: any = {}
  routeList = [];

  constructor(public router: Router,public commonServices:CommonService, public activatedRoute: ActivatedRoute) {
    // this.activatedRoute.queryParams.subscribe((res) => {
    //   console.log(res);
    this.route = commonServices.getSelectedCountry();
    // });
    console.log(this.route);
    this.routeList= this.route.routes;
  }
  ngOnInit() {
  }

  routeClick(item) {
    this.commonServices.setSelectedRoute(item);
    this.router.navigate(['/route-details'], {
      queryParams: item,
    });
    // this.router.navigateByUrl('/route-details');
  }
}
