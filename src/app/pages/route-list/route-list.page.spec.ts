import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouteListPage } from './route-list.page';

describe('RouteListPage', () => {
  let component: RouteListPage;
  let fixture: ComponentFixture<RouteListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RouteListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RouteListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
