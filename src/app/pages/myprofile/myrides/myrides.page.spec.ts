import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyridesPage } from './myrides.page';

describe('MyridesPage', () => {
  let component: MyridesPage;
  let fixture: ComponentFixture<MyridesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyridesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyridesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
