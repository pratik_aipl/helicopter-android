import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Tools } from 'src/app/tools';
import { CommonService } from 'src/app/shared/common.service';

@Component({
  selector: 'app-myrides',
  templateUrl: './myrides.page.html',
  styleUrls: ['./myrides.page.scss'],
})
export class MyridesPage implements OnInit {
  rideList = [];
  fromHome;
  constructor(
    public router: Router, public route: ActivatedRoute,
    public tools: Tools, public commonService: CommonService) {

    this.route.params
      .subscribe((params) => {
        console.log('params =>', params.page);
        this.fromHome = params.page;
      });

  }
  ngOnInit() {
  }
  ionViewDidEnter(){
    console.log('ionViewDidEnter --2 ');
    this.getRideHoistorylist();
  }
  getRideHoistorylist() {
    this.tools.openLoader();
    this.commonService.getRideHistory().subscribe(response => {

      let res: any = response;

      console.log('Data =>', res.data);
      console.log('Data => RideHoistorylist success ',res.ride_list);
      this.tools.closeLoader();
      this.rideList = (res.ride_list);
    }, (error: Response) => {
      console.log('Data => RideHoistorylist error ',error);
      console.log(error);
      this.tools.closeLoader();
      let err:any = error;
      this.tools.openAlertToken(err.status, err.error.message);
    });
  }
  rideClick(item) {
    //  this.router.navigateByUrl('/route-details');
   //  this.router.navigateByUrl('/submit-ratting');
  }
  rideRating(item) {
    //  this.router.navigateByUrl('/route-details');
    this.commonService.setSelRide('');
    this.commonService.setSelRide(item);
    if(this.fromHome !=undefined)
     this.router.navigateByUrl('/submit-ratting/0');
     else
     this.router.navigateByUrl('/submit-ratting/1');
  }
}
