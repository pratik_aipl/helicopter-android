import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyprofilePage } from './myprofile.page';
import { SharedModule } from 'src/app/shared/shared.module';


const routes: Routes = [
  {
    path: "tabs",
    component: MyprofilePage,
    children: [
      {
        path: "",
        redirectTo: "tabs/(mydetails:mydetails)",
        pathMatch: "full"
      },
      {
        path: "mydetails",
        children: [
          {
            path: "",
            loadChildren: "../myprofile/mydetails/mydetails.module#MydetailsPageModule"
          }
        ]
      },
      {
        path: "myride",
        children: [
          {
            path: "",
            loadChildren:
              "../myprofile/myrides/myrides.module#MyridesPageModule"
          }
        ]
      }
    ]
  },
  {
    path: "",
    redirectTo: "tabs/mydetails",
    pathMatch: "full"
  }
];

// const routes: Routes = [
//   {
//     path: '',
//     component: MyprofilePage,
//     children:[
//       { path: 'mydetails', loadChildren: '../myprofile/mydetails/mydetails.module#MydetailsPageModule' },
//       { path: 'myrides', loadChildren: '../myprofile/myrides/myrides.module#MyridesPageModule' },
//   ]
//   },{
//     path:'',
//     redirectTo:'../myprofile/mydetails',
//     pathMatch:'full'
//   }
// ];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MyprofilePage]
})
export class MyprofilePageModule {}
