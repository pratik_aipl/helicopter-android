import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/shared/common.service';
import { Events } from '@ionic/angular';

@Component({
  selector: 'app-myprofile',
  templateUrl: './myprofile.page.html',
  styleUrls: ['./myprofile.page.scss'],
})
export class MyprofilePage implements OnInit {

  user: any = {};
  constructor(public events: Events,public commonService: CommonService) {
    this.user = this.commonService.getUserData();
    console.log('get User Data ', this.user);
    events.subscribe('profileUpdate', (item) => {
      this.user= item;
      console.log('Event call')
    });
  }
  ngOnInit() {
  }

}
