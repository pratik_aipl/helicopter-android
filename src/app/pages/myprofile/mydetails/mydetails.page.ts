import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/shared/common.service';
import { Router } from '@angular/router';
import { Events } from '@ionic/angular';

@Component({
  selector: 'app-mydetails',
  templateUrl: './mydetails.page.html',
  styleUrls: ['./mydetails.page.scss'],
})
export class MydetailsPage implements OnInit {
  user: any = {};
  constructor(public router: Router,public events: Events,public commonService: CommonService) {
    this.user = this.commonService.getUserData();
    console.log('get User Data ', this.user);
    events.subscribe('profileUpdate', (item) => {
      this.user= item;
      console.log('Event call')
    });
  }

  ngOnInit() {
  }
  editProfile(){
    this.router.navigateByUrl('/editprofile');
  }
}
