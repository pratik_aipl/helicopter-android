import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/shared/common.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ActionSheetController, Events } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Tools } from 'src/app/tools';

@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.page.html',
  styleUrls: ['./editprofile.page.scss'],
})
export class EditProfilePage implements OnInit {
  user: any = {};
  countyCode: any;
  image1: string = '';
  file: any;
  images: any;
  form: FormGroup;
  isEdit = false;

  constructor(public commonService: CommonService, public router: Router,
    public actionSheetController: ActionSheetController, private tools: Tools,
    private camera: Camera, public events: Events, private webview: WebView,
    public formBuilder: FormBuilder,) {
    this.user = this.commonService.getUserData();
    console.log('get User Data ', this.user);
    this.countyCode = this.user.CountryCode
    this.form = this.formBuilder.group({
      //        image1: ['', Validators.required],
      fname: [this.user.firstname, [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(4), Validators.maxLength(10)]],
      lname: [this.user.lastname, Validators.required],
      email: [this.user.email, [Validators.required, Validators.email]],
      mphone: [this.user.mobileno, Validators.required],
      // pass: ['', [Validators.required, Validators.minLength(6)]],
      // cpass: ['', [Validators.required, Validators.minLength(6)]]
    });

  }


  async selectImage(type) {
    const actionSheet = await this.actionSheetController.create({
      header: "Select Image",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          if (type == '1') {
            this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          if (type == '1') {
            this.pickImage(this.camera.PictureSourceType.CAMERA);
          }
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }

  pickImage(sourceType) {
    const options: CameraOptions = {
      quality: 10,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      // console.log('User Image --> ', imageData);
      this.image1 = imageData;
    }, (err) => {
      console.log(err);
    });
  }
  ngOnInit() {
  }
  selectContry(countyCode) {
    console.log('County Code ', countyCode);
    this.countyCode = countyCode;
  }

  save() {

    if (this.countyCode == undefined || this.countyCode == '') {
      this.tools.openAlert('Please select country code');
    } else {
      this.tools.openLoader();
      let postData = new FormData();
      postData.append('EmailID', this.form.get('email').value);
      postData.append('FirstName', this.form.get('fname').value);
      postData.append('LastName', this.form.get('lname').value);
      postData.append('MobileNo', this.form.get('mphone').value);
      postData.append('CountryCode', this.countyCode);
      // postData.append("ProfileURL", this.image1?this.image1:'');
      if (this.image1) {
        const date = new Date().valueOf();
        // Replace extension according to your media type
        const imageName = date + '.jpeg';
        // call method that creates a blob from dataUri
        var imageBlob = this.commonService.dataURItoBlob(this.image1);
        // const imageFile = new File([imageBlob], imageName, { type: 'image/jpeg' })
        postData.append('ProfileURL', imageBlob, imageName);
      }
      this.commonService.editProfile(postData, this.image1).subscribe(data => {
        this.tools.closeLoader();
        let res: any = data;
        console.log('Edit Profile -->0 ', data);
        console.log('Edit Profile -->2 ', this.commonService.getLoginToken());
        // this.commonService.setEPass(JSON.parse(data.data).data.pass[0]);
        // this.commonService.setUserData(JSON.parse(data.data).data.user,JSON.parse(data.data).login_token);

        this.commonService.setUserData(res.data.user, this.commonService.getLoginToken());
        this.events.publish('profileUpdate', this.commonService.getUserData());
        this.router.navigateByUrl('/myprofile', { replaceUrl: true });
      }, (error: Response) => {
        this.tools.closeLoader();
        let err: any = error;
        console.log('Edit Profile -->0 ', err);
        this.tools.openAlertToken(err.status, err.error.message);

      });
    }

  }


}
