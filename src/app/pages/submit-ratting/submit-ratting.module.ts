import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StarRatingModule } from 'ionic4-star-rating';
import { SubmitRattingPage } from './submit-ratting.page';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: SubmitRattingPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    FormsModule,
    StarRatingModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SubmitRattingPage]
})
export class SubmitRattingPageModule {}
