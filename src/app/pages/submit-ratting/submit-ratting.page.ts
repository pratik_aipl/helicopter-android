import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';

@Component({
  selector: 'app-submit-ratting',
  templateUrl: './submit-ratting.page.html',
  styleUrls: ['./submit-ratting.page.scss'],
})
export class SubmitRattingPage implements OnInit {

  ride: any;
  rat = 0;
  cmt = '';
  fromHome;
  isShow=true;
  @ViewChild('rating') rating : any;
  constructor( public router: Router, public route: ActivatedRoute,public tools: Tools, public commonServices: CommonService, public activatedRoute: ActivatedRoute) {
    this.ride = commonServices.getSelRide();
    console.log('Selected Ride ', this.ride);
    this.route.params
      .subscribe((params) => {
        console.log('params =>', params.page);
        this.fromHome = params.page;
      });

      if(this.ride.ratings != null){
        this.rat = this.ride.ratings.Rating;
        this.cmt = this.ride.ratings.Comments;
        this.isShow=false;
      }
  }

  ngOnInit() {
  }
  logRatingChange(rating) {
    console.log("changed rating: ", rating);
    this.rat = rating;
  }
  submit() {

      this.tools.openLoader();
    let params = {
      'MyRideID':this.ride.MyRideID,
      'Rating':this.rat,
      'Comments':this.cmt,
      'PilotID':this.ride.pilot.id    
    }
      this.commonServices.rideRatingPost(params).subscribe(data => {
        this.tools.closeLoader();     
        console.log(data);
        if(this.fromHome == '0'){
          this.router.navigateByUrl('/myrides/home');
        }else{        
          this.router.navigateByUrl('/myprofile');
        }
      }, (error: Response) => {
        console.log(error);
        this.tools.closeLoader();
      let err:any = error;
      this.tools.openAlertToken(err.status, err.error.message);
      });
    }
}
