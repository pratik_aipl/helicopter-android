import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmitRattingPage } from './submit-ratting.page';

describe('SubmitRattingPage', () => {
  let component: SubmitRattingPage;
  let fixture: ComponentFixture<SubmitRattingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmitRattingPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmitRattingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
