import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/shared/common.service';

@Component({
  selector: 'app-confirm-bookings',
  templateUrl: './confirm-bookings.page.html',
  styleUrls: ['./confirm-bookings.page.scss'],
})
export class ConfirmBookingsPage implements OnInit {
selPass:any;
  constructor(public commonServices:CommonService) {
    this.selPass = this.commonServices.getSelPass();
   }

  ngOnInit() {
  }

}
