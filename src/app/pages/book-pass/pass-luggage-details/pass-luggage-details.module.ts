import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SharedModule } from 'src/app/shared/shared.module';
import { PassLuggageDetailsPage } from './pass-luggage-details.page';

const routes: Routes = [
  {
    path: '',
    component: PassLuggageDetailsPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PassLuggageDetailsPage]
})
export class PassLuggageDetailsPageModule {}
