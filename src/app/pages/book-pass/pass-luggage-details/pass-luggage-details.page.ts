import { DomSanitizer } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModalComponent } from '../modal/modal.component';
import { ModalController, ActionSheetController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import {
  InAppBrowser,
  InAppBrowserOptions,
} from "@ionic-native/in-app-browser/ngx";
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-pass-luggage-details',
  templateUrl: './pass-luggage-details.page.html',
  styleUrls: ['./pass-luggage-details.page.scss'],
})
export class PassLuggageDetailsPage implements OnInit {

  options: InAppBrowserOptions = {
  };

  cardDetails: any = {};
  idProof: any = '';
  selPass: any;
  page: any;
  form: FormGroup;
  currency: string = 'USD';
  passBagList = [];
  BagList=[];
  GetData=[];
  sumData = 0;

  constructor(public router: Router,
    public formBuilder: FormBuilder,  
      public actionSheetController: ActionSheetController, 
    private tools: Tools,  private iab: InAppBrowser,
    private camera: Camera,
    public route: ActivatedRoute,
    public imagePicker: ImagePicker, public modalController: ModalController,
    public commonServices: CommonService) {
      this.route.params
      .subscribe((params) => {
        console.log('params =>', params.page);
        this.page = params.page;
      });
    this.selPass = this.commonServices.getSelectedPass();

      for (let i = 0; i <= 10; i++) {

        if(i==0){
          this.BagList.push({key:"Select Bag",val:i})
        }else{
          this.BagList.push({key:i,val:i})

        }
    }

  
    this.form = this.formBuilder.group({
      //image1: ['', Validators.required],
      //idProof: ['', Validators.required],//[Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(4), Validators.maxLength(10)]
      // idProof: [''],//[Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(4), Validators.maxLength(10)]
     
    });

  
  }

  ngOnInit() {

  }

  ionViewDidEnter() {
    this.getBugDetailsList();
  }
  
  total(){
    //let sumData = 0;
    this.sumData=0;
      for (let i = 0; i < this.passBagList.length; i++) {
        const product = this.passBagList[i];
        this.sumData += (parseFloat(product.PricePerBag)*(parseInt(product.selbag != undefined? product.selbag:'0')));
      } 
    return this.sumData;
  }

  getBugDetailsList() {
    this.tools.openLoader();
    // let  postData = new FormData();
    // postData.append('PassID',this.selPass.PassID);
    this.commonServices.GetBagPass().subscribe(data => {
      this.tools.closeLoader();
      let res: any = data;
      console.log('Bag Pass List --> ',res.data)
      this.passBagList = [];
      for (let i = 0; i < res.data.length; i++) {
        const element = res.data[i];
        element.selbag='0'
        this.passBagList.push(element);
        
      }

    }, (error: Response) => {
      console.log(error);
      this.tools.closeLoader();
      let err:any = error;
      this.tools.openAlertToken(err.status, err.error.message);
    });
  }

  bookNow() {
    for (let i = 0; i < this.passBagList.length; i++) {
      const product = this.passBagList[i];
      console.log("id",product.LuggagePassID)
      console.log(product.selbag != undefined? product.selbag:'0','selBag')
      this.GetData.push({Id:product.LuggagePassID,val:product.selbag})
    } 
    console.log("GetData >> ",this.GetData)
    this.router.navigateByUrl('paymentoption/' + this.sumData + '/' + this.page+'/'+JSON.stringify(this.GetData)); 
  }

}
