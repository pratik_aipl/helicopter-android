import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';

@Component({
  selector: 'app-book-pass',
  templateUrl: './book-pass.page.html',
  styleUrls: ['./book-pass.page.scss'],
})
export class BookPassPage implements OnInit {
  passList = [];
  constructor(
    public commonService: CommonService,
    public tools: Tools,
    private router: Router) {
  }

  ngOnInit() {
  }
  ionViewDidEnter() {
    this.getCountryList();
  }
  getCountryList() {
    this.tools.openLoader();
    let countryId =this.commonService.getSelectedCountry()?this.commonService.getSelectedCountry().CountryID:'';
    this.commonService.passList(countryId).subscribe(data => {
      this.tools.closeLoader();
      let res: any = data;
      console.log('Pass List --> ',res.data)
      this.passList = res.data;
    }, (error: Response) => {
      console.log(error);
      this.tools.closeLoader();
      let err:any = error;
      this.tools.openAlertToken(err.status, err.error.message);
    });
  }
  passClick(item) {
    this.commonService.setSelectedPass(item);
    if(item.PassType=="Luggage"){
      this.router.navigateByUrl('pass-luggage-details/book');
    }else{
    this.router.navigateByUrl('/pass-details/book');
    }
    // this.router.navigateByUrl('/pass-details', { replaceUrl: true });
  }
}
