import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PassDetailsPage } from './pass-details.page';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: PassDetailsPage
   }
  //,{
  //   path: 'modal',
  //   component: ModalComponent
  // },
];

@NgModule({
  //entryComponents: [ModalComponent],
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
 // declarations: [PassDetailsPage,ModalComponent]
  declarations: [PassDetailsPage]
})
export class PassDetailsPageModule {}
