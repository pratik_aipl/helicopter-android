import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModalComponent } from '../modal/modal.component';
import { ModalController, ActionSheetController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import {
  InAppBrowser,
  InAppBrowserOptions,
} from "@ionic-native/in-app-browser/ngx";
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-pass-details',
  templateUrl: './pass-details.page.html',
  styleUrls: ['./pass-details.page.scss'],
})
export class PassDetailsPage implements OnInit {

  options: InAppBrowserOptions = {
  };

  imageFilePath: any;
  cardDetails: any = {};
  idProof: any = '';
  image1: any;
  selPass: any;
  page: any;
  form: FormGroup;
  currency: string = 'USD';
  constructor(public router: Router,
    public formBuilder: FormBuilder,  
    public actionSheetController: ActionSheetController, 
    private tools: Tools,  private iab: InAppBrowser,
    private camera: Camera,
    public route: ActivatedRoute,
    public imagePicker: ImagePicker, public modalController: ModalController,
    public commonServices: CommonService) {
      this.route.params
      .subscribe((params) => {
        console.log('params =>', params.page);
        this.page = params.page;
      });
    this.selPass = this.commonServices.getSelectedPass();

    this.form = this.formBuilder.group({
      //image1: ['', Validators.required],
      idProof: ['', Validators.required],//[Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(4), Validators.maxLength(10)]
      // idProof: [''],//[Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(4), Validators.maxLength(10)]
    });
  }

  ngOnInit() {

  }

  async selectImage(type) {
    const actionSheet = await this.actionSheetController.create({
      header: "Select Image",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          if (type == '1') {
            this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          if (type == '1') {
            this.pickImage(this.camera.PictureSourceType.CAMERA);
          }
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }

  pickImage(sourceType) {
    const options: CameraOptions = {
      quality: 100,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      console.log('User Image --> ', imageData);
      this.image1 = imageData;
    }, (err) => {
      console.log(err);
    });
  }

  onPickDock() {
    const options = {
      maximumImagesCount: 1,
      width: 800,
      height: 800,
      quality: 50,
      outputType: 1 //Set output type to 1 to get base64img
    };
    this.imagePicker.getPictures(options).then((results) => {
      for (var i = 0; i < results.length; i++) {
        this.image1 = results[i];
      }
    }, (err) => {
      console.log(err);
    });
  }

  bookNow() {
    this.router.navigateByUrl('paymentoption/' + this.form.get('idProof').value + '/' + this.page+'/'+this.image1);    
  }

}
