import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookPassPage } from './book-pass.page';

describe('BookPassPage', () => {
  let component: BookPassPage;
  let fixture: ComponentFixture<BookPassPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookPassPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookPassPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
