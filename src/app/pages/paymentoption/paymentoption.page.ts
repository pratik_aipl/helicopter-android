import { ModalController } from '@ionic/angular';
import { Tools } from './../../tools';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/shared/common.service';
import { environment } from 'src/environments/environment';
import { ModalComponent } from '../book-pass/modal/modal.component';

@Component({
  selector: 'app-paymentoption',
  templateUrl: './paymentoption.page.html',
  styleUrls: ['./paymentoption.page.scss'],
})
export class PaymentOptionPage implements OnInit {
  options: InAppBrowserOptions = {
  };
  
selPass:any;
data:any='Paypal';
idProof: any = '';
page: any;
image1: any;

  constructor(public commonServices:CommonService,
    public router: Router,public route: ActivatedRoute,
    private tools: Tools,  private iab: InAppBrowser,
    public modalController: ModalController) 
    {
      this.route.params.subscribe((params) => {
        console.log('params page =>', params.page);
        this.page = params.page;

        console.log('params idproof =>', params.idproof);
        this.idProof = params.idproof;
        
        console.log('params image1 =>', params.image1);
        this.image1 = params.image1;


      });

      this.selPass = this.commonServices.getSelectedPass();
      console.log("data >>",this.selPass)

   }

  ngOnInit() {
  }

  onChangeValue($event) {
    this.data = $event.target.value;
    console.log("data >>",this.data)
   
  }

  paynow() {
    this.tools.openLoader();

    let  postData = new FormData();

    postData.append('item_name', this.selPass.PassName);
    postData.append('amt',this.selPass.PassType=="Luggage"? this.idProof:this.selPass.PassAmount);
    postData.append('PassID',this.selPass.PassID);
    postData.append('PassType',this.selPass.PassType);
    postData.append('LuggageList',this.image1);

    //console.log("data pass type >>",this.selPass.PassType)

  
    this.commonServices.payPass(postData).subscribe(data => {
      this.tools.closeLoader();
      var statuspay = false;
      let targetPay = "_blank";
      let res: any = data;
      console.log('Pay Api Data  >> ',res);

      if(res.status){
        var token, PayerID;
        const browserPay = this.iab.create(res.data.url, targetPay, this.options);
        browserPay.on("loadstart").subscribe((event) => {
          // console.log('Pay Data loadstart url ',event.url);
                });
        browserPay.on("loadstop").subscribe((event) => {
          console.log('browserPay Pay Data url >> ',event.url);
          var url = new URL(event.url);
          token = url.searchParams.get("token");
          PayerID = url.searchParams.get("PayerID");
          // statuspay = ("http://"+url.hostname+url.pathname) == (environment.BaseUrl + "paypalVerifyMobile")
          //     https://backend.iflyhto.com/api/v1/user/PaypalVerifyMobile?token=EC-3NJ997630G535784A&PayerID=WXPK8U6KZ4KJ4
          
          if(this.selPass.PassType=="Luggage"){
            statuspay = (event.url == (environment.BaseUrl + "PaypalVerifyLuggage?token="+token+"&PayerID="+PayerID))
            console.log('statuspay Luggage Pay Data url >> ',(environment.BaseUrl + "PaypalVerifyLuggage?token="+token+"&PayerID="+PayerID));
          }else{
            statuspay = (event.url == (environment.BaseUrl + "PaypalVerifyMobile?token="+token+"&PayerID="+PayerID))
            console.log('statuspay user Pay Data url >>',(environment.BaseUrl + "PaypalVerifyMobile?token="+token+"&PayerID="+PayerID));

          }
            console.log('statuspay ',statuspay);
             if (statuspay) {                 
                browserPay.close();
                  }
                });
        browserPay.on("exit").subscribe((event) => {
            if (statuspay) {
              console.log('PassType >>> ',this.selPass.PassType);
              if(this.selPass.PassType=="Luggage"){
                this.callPayluggage(PayerID,token)
              }else{
                this.callPay(PayerID,token)
              }
            } else {
              // this.isClick = false;
              this.tools.presentAlert("", 'Payment Cancelled','Ok');
            }
          }
        );
  
      }else{
        this.tools.presentAlert("", res.message,'Ok');
      }

    }, (error: Response) =>  {
      this.tools.closeLoader();
      let err:any = error;
      console.log('Booking error --> ',err.error);
      this.tools.openAlertToken(err.status, err.error.message);

    });
  }

  callPay(payerid,token) {
    this.tools.openLoader();

    const date = new Date().valueOf();
    // Replace extension according to your media type
    const imageName = date + '.jpeg';


    let  postData = new FormData();
    postData.append('PassID',this.selPass.PassID);
    postData.append('amt',this.selPass.PassAmount);
    postData.append("PayerID", payerid);
    postData.append("token", token);
    postData.append('PersonalorFamily','Family');
    postData.append('IDProof', this.idProof);
    postData.append('IDProofDoc', this.image1);
    postData.append('TransactionID', token+"^"+payerid);
   
    this.commonServices.paypalVerifyMobile(postData).subscribe(data => {
      this.tools.closeLoader();
      // console.log(JSON.parse(data.data).data);
      let res: any = data;
      console.log('Pay Data Api ',res);

      if(res.status){
        if(this.page == 'purchase'){
          this.Emergency();
        }else{
          this.router.navigateByUrl('/mybookings', { replaceUrl: true });
        }
      }
    }, (error: Response) =>  {
      this.tools.closeLoader();
      let err:any = error;
      console.log('Booking error --> ',err);
      this.tools.openAlertToken(err.status, err.error.message);
 
    });
  }

  callPayluggage(payerid,token) {
    this.tools.openLoader();

    let  postData = new FormData();
    postData.append('PassID',this.selPass.PassID);
    postData.append('amt',this.idProof);
    postData.append('PassType',this.selPass.PassType);
    postData.append('LuggageList',this.image1);
    postData.append("PayerID", payerid);
    postData.append("token", token);
    postData.append('PersonalorFamily','Family');
    postData.append('TransactionID', token+"^"+payerid);
   
    this.commonServices.PaypalVerifyLuggage(postData).subscribe(data => {
      this.tools.closeLoader();
      // console.log(JSON.parse(data.data).data);
      let res: any = data;
      console.log('Pay Data Api ',res);

      if(res.status){
        if(this.page == 'purchase'){
          this.Emergency();
        }else{
          this.router.navigateByUrl('/mybookings', { replaceUrl: true });
        }
      }
    }, (error: Response) =>  {
      this.tools.closeLoader();
      let err:any = error;
      console.log('Booking error --> ',err);
      this.tools.openAlertToken(err.status, err.error.message);
 
    });
  }

  async bookNowStrip() {
    const modal = await this.modalController.create({
      component: ModalComponent,
      cssClass: 'modal',
      componentProps: { value: this.selPass.PassAmount }
    });
    await modal.present();
    await modal.onDidDismiss()
      .then((data) => {
        console.log(data);
        if (data.data) {
         // this.callPay(data.data);
          
          //this.answer.answers[this.currquekey].file = data.data;        
        }
      });
  }

  Emergency(){
    this.tools.openLoader();
    this.commonServices.Emergency().subscribe(response => {
      this.tools.closeLoader();
      let res: any = response;
      this.tools.openAlert(res.message);
      this.router.navigateByUrl('/mybookings', { replaceUrl: true });
    }, (error: Response) => {
      this.tools.closeLoader();
      console.log(error);
      let err:any = error;
      this.tools.openAlertToken(err.status, err.error.message);
    });
  }

}
