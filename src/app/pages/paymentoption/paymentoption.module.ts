import { ModalComponent } from './../book-pass/modal/modal.component';
import { PaymentOptionPage } from './paymentoption.page';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: PaymentOptionPage
  },{
    path: 'modal',
    component: ModalComponent
  },
];

@NgModule({
  entryComponents: [ModalComponent],
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PaymentOptionPage,ModalComponent]

})
export class PaymentOptionPageModule {}
