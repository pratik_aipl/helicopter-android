import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';

@Component({
  selector: 'app-thank-modal',
  templateUrl: './thank-modal.component.html',
  styleUrls: ['./thank-modal.component.scss'],
})
export class ThankModalComponent implements OnInit {
paramData:any;
QRCode:any;
  Tittle:any;
  constructor( public navParams: NavParams,
    public modalCtrl: ModalController,public commonService:CommonService,public tools:Tools) {
      this.QRCode = this.navParams.get('value');
      this.Tittle = this.navParams.get('tittle');
  console.log('tittle >>',this.navParams.get('tittle')  );
     }

  ngOnInit() {}

  close() {
    this.modalCtrl.dismiss();
  }

}
