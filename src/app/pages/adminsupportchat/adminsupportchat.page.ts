import { CommonService } from './../../shared/common.service';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Events, ToastController } from '@ionic/angular';
import { Tools } from 'src/app/tools';
@Component({
  selector: 'app-chat',
  templateUrl: './adminsupportchat.page.html',
  styleUrls: ['./adminsupportchat.page.scss'],
})
export class AdminSupportChatPage implements OnInit {
 

  message = '';
  messages = [];
  topPoint=50;

  @ViewChild('content') private content: any;
  @ViewChild('inMsg') yourElement: ElementRef;

  constructor(private tools: Tools,public events: Events, public commonService: CommonService, private activatedRoute: ActivatedRoute, private toastCtrl: ToastController) {

    
  }

  
  ngOnInit() {
    this.getsupportmassages();
  } 
  ionViewDidEnter() {
   // this.getsupportmassages();
  }

  scrollFunction(ev) {
   this.topPoint=ev.detail.scrollTop
   }

   getsupportmassages() {
    this.tools.openLoader();
    this.commonService.getsupportchat().subscribe(data => {
      this.tools.closeLoader();
      let res: any = data;
      this.messages = res.data;
      console.log('Data => support message ',  this.messages); 
      // let res: any = data;

      // console.log(' support message ', res);

    }, (error: Response) => {
      console.log(error);
      this.tools.closeLoader();
      let err:any = error;
      this.tools.openAlertToken(err.status, err.error.message);
    });
  }
 

  onChangeTime(data): void {
    console.log("onChangeTime to time: " + this.message + ". Event data: " + data);
    //this.socket.emit("typing", { msg: this.message, senderID: this.commonService.getUserId(), receiverID: this.receiverID });
    setTimeout(() => {
     // this.socket.emit("stopTyping", { msg: this.message, senderID: this.commonService.getUserId(), receiverID: this.receiverID });
    }, 1000);
  }

  sendMessage() {
    this.tools.openLoader();
    let  postData = new FormData();
    postData.append('Mesaage',this.message);
    this.commonService.SuppportSendMassage(postData).subscribe(data => {
      this.tools.closeLoader();
      let res: any = data;
      console.log('Pass List --> ',res.data)
      const msg = {
      CreatedBy: this.commonService.getUserId(),
      Mesaage: this.message,
      MessageDate: new Date(),
    }
    this.messages.push(msg);
    this.message = '';
    this.scrollToLatestMessage();
    }, (error: Response) => {
      console.log(error);
      this.tools.closeLoader();
      let err:any = error;
      this.tools.openAlertToken(err.status, err.error.message);
    });
  }

  ionViewWillLeave() {
    console.log('ionViewWillLeave ');
    this.events.publish('msgCount'); 
  }

  scrollToLatestMessage(): void {
    setTimeout(() => {
      this.content.scrollToBottom(1500);
    }, 300);
  }


  async showToast(msg) {
    let toast = await this.toastCtrl.create({
      message: msg,
      position: 'top',
      duration: 2000
    });
    toast.present();
  }
}
