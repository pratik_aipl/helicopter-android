import { AdminSupportChatPage } from './adminsupportchat.page';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';


describe('ChatPage', () => {
  let component: AdminSupportChatPage;
  let fixture: ComponentFixture<AdminSupportChatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminSupportChatPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSupportChatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
