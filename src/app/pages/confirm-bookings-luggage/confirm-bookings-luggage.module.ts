import { ThankModalComponent } from './../thank-modal/thank-modal.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { SharedModule } from 'src/app/shared/shared.module';
import { ConfirmBookingsLuggagePage } from './confirm-bookings-luggage.page';

const routes: Routes = [
  {
    path: '',
    component: ConfirmBookingsLuggagePage
  },{
    path: 'thank-model',
    component: ThankModalComponent
  },
];

@NgModule({
  entryComponents: [ThankModalComponent],

  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ConfirmBookingsLuggagePage,ThankModalComponent]
})
export class ConfirmBookingsLuggagePageModule {}
