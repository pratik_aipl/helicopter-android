import { ThankModalComponent } from './../thank-modal/thank-modal.component';
import { ModalController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { Tools } from 'src/app/tools';
import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/shared/common.service';

@Component({
  selector: 'app-confirm-bookings-luggage',
  templateUrl: './confirm-bookings-luggage.page.html',
  styleUrls: ['./confirm-bookings-luggage.page.scss'],
})
export class ConfirmBookingsLuggagePage implements OnInit {
  sliderOpts = {
    zoom: false,
    slidesPerView: 1.5,
    spaceBetween: 20,
    centeredSlides: true
  };
selPass:any;
bookingBagList = [];
PassID:any;
  constructor(private modalCtrl: ModalController,public tools: Tools, public commonServices: CommonService,public route: ActivatedRoute) {

    this.route.params.subscribe((params) => {
      console.log('params PassId =>', params.PassID);
      this.PassID = params.PassID;

    });
    this.selPass = this.commonServices.getSelPass();
   }

  ngOnInit() {
  }
  ionViewDidEnter() {
    this.getBookedBagList();
  }
  async showQRCode(QRCode,Tittle) {
    console.log("Tittle",Tittle)
    const modal = await this.modalCtrl.create({
      component: ThankModalComponent,

      // cssClass: 'transparent-modal',
      cssClass: 'thank-modal',
      componentProps: { value: QRCode,tittle: Tittle}
    });
    await modal.present();
    await modal.onDidDismiss()
      .then((data) => {
        console.log(data);
        if (data.data) {
         // this.callPay(data.data);
          
          //this.answer.answers[this.currquekey].file = data.data;        
        }
      });
  }

  getBookedBagList() {
    this.tools.openLoader();
    this.commonServices.bookedbaglist(this.PassID).subscribe(data => {
      this.tools.closeLoader();
      let res: any = data;
      console.log('Data => bookingBagList ', res);
      this.bookingBagList = res.data;
    }, (error: Response) => {
      console.log('bookedBag ==> ',error)
      console.log(error);
      this.tools.closeLoader();
      let err:any = error;
      this.tools.openAlertToken(err.status, err.error.message);      
    });
  }
}
