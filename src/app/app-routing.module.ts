import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './shared/authguard.service';

const routes: Routes = [
  // { path: '', redirectTo: 'submit-ratting', pathMatch: 'full' },
  { path: '', redirectTo: 'splash', pathMatch: 'full' },
  // {
  //   path: 'home',
  //   loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  // },
  { path: 'home', canActivate: [AuthGuard], loadChildren: './home/home.module#HomePageModule' },
  { path: 'login', loadChildren: './auth/login/login.module#LoginPageModule' },
  { path: 'register', loadChildren: './auth/register/register.module#RegisterPageModule' },
  { path: 'route-list', loadChildren: './pages/route-list/route-list.module#RouteListPageModule' },
  { path: 'route-details', loadChildren: './pages/route-details/route-details.module#RouteDetailsPageModule' },
  { path: 'myprofile', canActivate: [AuthGuard], loadChildren: './pages/myprofile/myprofile.module#MyprofilePageModule' },
  { path: 'editprofile', canActivate: [AuthGuard], loadChildren: './pages/myprofile/mydetails/editprofile/editprofile.module#EditProfilePageModule' },
  { path: 'our-services', loadChildren: './pages/our-services/our-services.module#OurServicesPageModule' },
  { path: 'mybookings', canActivate: [AuthGuard], loadChildren: './pages/mybookings/mybookings.module#MybookingsPageModule' },
  { path: 'confirm-bookings', canActivate: [AuthGuard], loadChildren: './pages/confirm-bookings/confirm-bookings.module#ConfirmBookingsPageModule' },
  { path: 'confirm-bookings-luggage/:PassID', canActivate: [AuthGuard], loadChildren: './pages/confirm-bookings-luggage/confirm-bookings-luggage.module#ConfirmBookingsLuggagePageModule' },
  { path: 'paymentoption/:idproof/:page/:image1', canActivate: [AuthGuard], loadChildren: './pages/paymentoption/paymentoption.module#PaymentOptionPageModule' },
  { path: 'book-pass', canActivate: [AuthGuard], loadChildren: './pages/book-pass/book-pass.module#BookPassPageModule' },
  { path: 'submit-ratting/:page', canActivate: [AuthGuard], loadChildren: './pages/submit-ratting/submit-ratting.module#SubmitRattingPageModule' },
  { path: 'pass-details/:page', canActivate: [AuthGuard], loadChildren: './pages/book-pass/pass-details/pass-details.module#PassDetailsPageModule' },
  { path: 'pass-luggage-details/:page', canActivate: [AuthGuard], loadChildren: './pages/book-pass/pass-luggage-details/pass-luggage-details.module#PassLuggageDetailsPageModule' },
  { path: 'myrides/:page', canActivate: [AuthGuard], loadChildren: './pages/myprofile/myrides/myrides.module#MyridesPageModule' },
  { path: 'verify-otp/:code/:mno', loadChildren: './auth/verify-otp/verify-otp.module#VerifyOtpPageModule' },
  { path: 'change-password', canActivate: [AuthGuard], loadChildren: './auth/change-password/change-password.module#ChangePasswordPageModule' },
  { path: 'forgot-password', loadChildren: './auth/forgot-password/forgot-password.module#ForgotPasswordPageModule' },
  // { path: 'chat', loadChildren:'./chat/chat.module#chatPageModule' },
  { path: 'chat/:type/:name/:userpic',canActivate: [AuthGuard], loadChildren: './chat/chat.module#ChatPageModule' },
  { path: 'groupchat/:id/:name/:isAdmin',canActivate: [AuthGuard], loadChildren: './groupchat/groupchat.module#GroupChatPageModule' },
  { path: 'chatuserlist', loadChildren: './chatuserlist/chatuserlist.module#ChatuserlistPageModule' },
  { path: 'splash', loadChildren: './splash/splash.module#SplashPageModule' },
  { path: 'adminsupportchat', canActivate: [AuthGuard], loadChildren: './pages/adminsupportchat/adminsupportchat.module#AdminSupportChatPageModule' },
  { path: 'setting',canActivate: [AuthGuard], loadChildren: './setting/setting.module#SettingPageModule' },
  { path: 'grouplist', canActivate: [AuthGuard],loadChildren: './grouplist/grouplist.module#GrouplistPageModule' },
  { path: 'creategroup',canActivate: [AuthGuard], loadChildren: './creategroup/creategroup.module#CreateGroupPageModule' },
  { path: 'addnewmember/:id',canActivate: [AuthGuard], loadChildren: './addnewmember/addnewmember.module#AddNewMemberPageModule' },
  { path: 'groupmemberlist/:id/:name/:isAdmin',canActivate: [AuthGuard], loadChildren: './groupmemberlist/groupmemberlist.module#GroupMemberListPageModule' },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
