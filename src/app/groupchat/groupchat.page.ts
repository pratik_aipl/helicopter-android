import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Events, ToastController } from '@ionic/angular';
import { Socket } from 'ngx-socket-io';
import { Tools } from 'src/app/tools';
import { CommonService } from '../shared/common.service';
@Component({
  selector: 'app-groupchat',
  templateUrl: './groupchat.page.html',
  styleUrls: ['./groupchat.page.scss'],
})
export class GroupChatPage implements OnInit {
 
  message = '';
  messages = [];
  currentUser = '';
  groupID: any;
  isAdmin: any;
  name: any;
  typing = false;
  task: any;
  user: any;
  from: any;
  page: any;
  topPoint=50;
  UserPic:any;
  @ViewChild('content') private content: any;
  @ViewChild('inMsg') yourElement: ElementRef;

  constructor(private tools: Tools, private socket: Socket,public events: Events,
     public commonService: CommonService, private activatedRoute: ActivatedRoute, 
     private toastCtrl: ToastController,public router: Router) {
   
    this.isAdmin = this.activatedRoute.snapshot.paramMap.get('isAdmin');
    this.groupID = this.activatedRoute.snapshot.paramMap.get('id');
    this.name = this.activatedRoute.snapshot.paramMap.get('name');
    this.user = this.commonService.getUserData();
     console.log("this.groupID  >>"+this.groupID);
     console.log("this.name  >>"+this.name);
  }

  
  ngOnInit() {
    this.callgetPendingMessage(true);
    this.socket.connect();
    this.currentUser = this.user.name;
    const data = {
      memberID: this.commonService.getUserId(),
      groupID: this.groupID,
      type: true
    }
    this.socket.emit('group-set-name', data);

    this.socket.on('connection', (socket) => { 
      console.log('Connection >>> ',socket)
    });
    
    this.socket.fromEvent('group-typing').subscribe(typing => {
      this.typing = true;
    });
    this.socket.fromEvent('group-stopTyping').subscribe(stopTyping => {
      console.log('Stop Typing --> ');
      this.typing = false;
    });
    this.socket.fromEvent('group chat message').subscribe(message => {
      console.log('chat msg -->',message);
      const msg = {
        MsgBy: this.groupID,
        Message: message,
        UpdatedAt: new Date(),
      }
      this.messages.push(msg);
      this.typing = false;
      if(this.topPoint>60){
        this.scrollToLatestMessage();
      }
      // this.grid.scrollToBottom();
    });


  }

  scrollFunction(ev) {
   this.topPoint=ev.detail.scrollTop
    // console.log('scroll event scrollTop', ev.detail.scrollTop);
    // console.log('scroll event detail', ev.detail);
   }

  callgetPendingMessage(isFill) {
    const data = {
      senderID: this.commonService.getUserId(),
      groupID:this.groupID,
     // TaskId: this.task.TaskID,
    }
    this.tools.openLoader();
    console.log('callgetPendingMessage ');

    this.commonService.groupPendingMessages(data).subscribe(data => {
      this.tools.closeLoader();
      let res: any = data;
       this.messages = res.messages;
      console.log('Data => userList ',  this.messages);
      // this.bookingList = res.data;
    }, (error: Response) => {
      console.log('bookedPass ==> ',error)
      console.log(error);
      this.tools.closeLoader();
      let err:any = error;
      this.tools.openAlertToken(err.status, err.error.message);      
    });

  }

  onChangeTime(data): void {
    console.log("onChangeTime to time: " + this.message + ". Event data: " + data);
    this.socket.emit("group-typing", { memberID: this.commonService.getUserId(), groupID: this.groupID });
    setTimeout(() => {
      this.socket.emit("group-stopTyping", {memberID: this.commonService.getUserId(), groupID: this.groupID });
    }, 1000);
  }

  sendMessage() {
    this.socket.emit("group chat message", { msg: this.message, memberID: this.commonService.getUserId(), groupID: this.groupID });
    this.socket.emit("msgCount", { memberID: this.commonService.getUserId(), groupID: this.groupID });
    // this.events.publish('msgCount'); 
    const msg = {
      MsgBy: this.commonService.getUserId(),
      Message: this.message,
      UpdatedAt: new Date(),
    }
    this.messages.push(msg);
    this.message = '';
    this.scrollToLatestMessage();
  }
  ionViewWillLeave() {
    console.log('ionViewWillLeave ');

    this.events.publish('msgCount'); 
   // this.callgetPendingMessage(false);
  //  this.socket.emit("msgCount", { MemberID: this.groupID, groupID: this.commonService.getUserId(), taskID: this.task.TaskID });
//    this.socket.disconnect();
  }

  scrollToLatestMessage(): void {
    setTimeout(() => {
      this.content.scrollToBottom(1500);
    }, 300);
  }


  async showToast(msg) {
    let toast = await this.toastCtrl.create({
      message: msg,
      position: 'top',
      duration: 2000
    });
    toast.present();
  }


  showmember(){
    this.router.navigateByUrl('/groupmemberlist/'+this.groupID+'/'+this.name+'/'+this.isAdmin);

  }
}
