import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';
import { ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs/internal/Subscription';
import { interval } from 'rxjs/internal/observable/interval';
@Component({
  selector: 'app-verify-otp',
  templateUrl: './verify-otp.page.html',
  styleUrls: ['./verify-otp.page.scss'],
})

export class VerifyOtpPage implements OnInit {


  private subscription: Subscription;
  total = (60000 * 2)
  shouldDisable = true;
  timeBtn = ''
  otp = ''
  mno = '';
  code = '';

  constructor(public router: Router, private route: ActivatedRoute,
    public commonService: CommonService, public modalController: ModalController,
    public tools: Tools) {
    this.route.params
      .subscribe((params) => {
        console.log('params =>', params.mno);
        this.code = params.code;
        this.mno = params.mno;
      });

    this.subscription = interval(100)
      .subscribe((val) => { this.getTimeDifference(val); });

    // this.subscription = interval(1)
    //   .subscribe(x => { this.getTimeDifference(val); });
  }
  getTimeDifference(val) {
    this.total = this.total - 100;
    if (this.total == 0) {
      this.shouldDisable = false;
      this.subscription.unsubscribe();
    }
    // console.log('Couner ', this.total );
  }
  millisToMinutesAndSeconds = (millis) => {
    var minutes = Math.floor(millis / 60000);
    var seconds = parseInt(Math.floor(((millis % 60000) / 1000)).toFixed(0));
    //ES6 interpolated literals/template literals 
    //If seconds is less than 10 put a zero in front.
    return `${(minutes < 10 ? "0" : "")}${minutes}:${(seconds < 10 ? "0" : "")}${seconds}`;
  }

  ngOnInit(): void {

  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  resendOtp() {
    if (this.mno.trim().length < 1) {
      this.tools.openAlert('Please enter mobile no.');
    } else {
      this.tools.openLoader();
      this.commonService.sendOtp(this.code, this.mno).subscribe(response => {
        this.tools.closeLoader();
        let res: any = response;
        // localStorage.setItem('login_token', res.login_token);
        // this.commonService.setEPass(res.data.pass[0]);
        // this.commonService.setUserData(res.data.user, res.login_token);
        // this.router.navigateByUrl('verify-otp/' + this.mno);
        this.shouldDisable = true
        this.total = (60000 * 2)
        this.subscription = interval(100)
          .subscribe((val) => { this.getTimeDifference(val); });
      }, (error: Response) => {
        console.log(error);
        this.tools.closeLoader();
        let err: any = error;
        this.tools.openAlertToken(err.status, err.error.message);
      });

    }
  }

  goToRegister() {
    this.router.navigateByUrl('/register');
  }

  onSubmit() {
    if (this.otp.trim().length < 1) {
      this.tools.openAlert('Please enter otp no.');
    } else {
      this.tools.openLoader();
      this.commonService.confirmopt(this.code, this.mno, this.otp).subscribe(response => {
        this.tools.closeLoader();
        let res: any = response;
        console.log(' response ', res);
        localStorage.setItem('login_token', res.data.login_token);
        // this.commonService.setEPass(res.data.pass[0]);
        this.tools.showToast(res.message, 2000);
        this.commonService.setUserData(res.data.user, res.data.login_token);
        this.router.navigateByUrl('/home', { replaceUrl: true });
        // this.router.navigateByUrl('verify-otp/' + mno);
      }, (error: Response) => {
        console.log(error);
        this.tools.closeLoader();
        let err: any = error;
        this.tools.openAlertToken(err.status, err.error.message);
      });
    }

  }

}
