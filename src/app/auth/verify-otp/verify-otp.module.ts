import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { VerifyOtpPage } from './verify-otp.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path: '',
    component: VerifyOtpPage
  }
];

@NgModule({
  imports: [
    SharedModule,FormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [VerifyOtpPage]
})
export class VerifyOtpPageModule {}
