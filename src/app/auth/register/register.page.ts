import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { DomSanitizer } from '@angular/platform-browser';
import { ActionSheetController, Events } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  image1: string;
  imageData;
  file: any;
  images: any;
  form: FormGroup;
  user: any = {};
  countyCode: any;
  MobNo: any;
  constructor(public router: Router,
    public formBuilder: FormBuilder,
    public commonService: CommonService,
    public imagePicker: ImagePicker,
    protected domSanitizer: DomSanitizer,
    public actionSheetController: ActionSheetController, private tools: Tools,
    private camera: Camera, public events: Events) {
    this.tools.callOneSignal();
    this.form = this.formBuilder.group({
      //        image1: ['', Validators.required],
      fname: ['', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(4), Validators.maxLength(10)]],
      lname: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      mphone: ['', Validators.required],
      pass: ['', [Validators.required, Validators.minLength(6)]],
      cpass: ['', [Validators.required, Validators.minLength(6)]],
      check: [false]
    });
  }
  ngOnInit() {
  }
  goToLogin() {
    this.router.navigateByUrl('/');
  }

  async selectImage(type) {
    const actionSheet = await this.actionSheetController.create({
      header: "Select Image",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          if (type == '1') {
            this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          if (type == '1') {
            this.pickImage(this.camera.PictureSourceType.CAMERA);
          }
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }

  pickImage(sourceType) {
    const options: CameraOptions = {
      quality: 100,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      // console.log('User Image --> ', imageData);
      this.imageData = imageData;
      this.image1 = imageData;
    }, (err) => {
      console.log(err);
    });
  }

  onTakePictures1() {

    const options = {
      maximumImagesCount: 1,
      width: 800,
      height: 800,
      quality: 100,
      outputType: 1 //Set output type to 1 to get base64img
    };

    this.imagePicker.getPictures(options).then((results) => {
      for (var i = 0; i < results.length; i++) {
        this.image1 = results[i];
        this.imageData = results[i];
      }
    }, (err) => {
      console.log(err);
    });
  }
  selectContry(countyCode) {
    console.log('County Code ', countyCode);
    this.countyCode = countyCode;
  }
  onSubmit() {

    if (this.form.get('check').value != false) {

      let pass = this.form.get('pass').value;
      let cpass = this.form.get('cpass').value;

      var dID;
      if (this.countyCode == undefined || this.countyCode == '') {
        this.tools.openAlert('Please select country code');
      } else if (pass != cpass) {
        this.tools.openAlert('Your password and confirmation password do not match.');
      } else {
        if (!localStorage.getItem('PlearID')) {
          dID = localStorage.getItem('PlearID') == '' ? "111111" : localStorage.getItem('PlearID');
        } else {
          dID = '111111'
        }
        this.tools.openLoader();

        let postData = new FormData();
        // postData.append('file', imageFile);
        postData.append("EmailID", this.form.get('email').value);
        postData.append("FirstName", this.form.get('fname').value);
        postData.append("LastName", this.form.get('lname').value);
        postData.append("CountryCode", this.countyCode);
        postData.append("MobileNo", this.form.get('mphone').value);
        postData.append("Password", this.form.get('pass').value);
        postData.append("ConfirmPassword", this.form.get('cpass').value);
       // postData.append("PlayerID", dID);
        // postData.append("ProfileURL", this.image1?this.image1:'');

        var imageBlob: any;
        if (this.imageData != undefined) {
          const date = new Date().valueOf();
          // Replace extension according to your media type
          const imageName = date + '.jpeg';
          imageBlob = this.commonService.dataURItoBlob(this.imageData);
          postData.append('ProfileURL', imageBlob, imageName);
          //  postData.append('ProfileURL', this.file, this.file.name);
        }
        this.commonService.register(this.user, postData).subscribe(response => {
          this.tools.closeLoader();
          let res: any = response;
          this.MobNo=this.form.get('mphone').value;
          this.form.reset();
          // localStorage.setItem('login_token', res.login_token);
          console.log("mphone >>>>",this.MobNo);
          this.router.navigateByUrl('/verify-otp/' + this.countyCode + '/' + this.MobNo);
        }, (error: Response) => {

          console.log(error);
          this.tools.closeLoader();
          let err: any = error;
          this.tools.openAlertToken(err.status, err.error.message);
        });
      }
    } else {
      this.tools.openAlert('Please accept terms and conditions.');
    }

  }

}
