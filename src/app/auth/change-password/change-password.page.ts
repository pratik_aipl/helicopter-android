import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';
import { ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs/internal/Subscription';
import { interval } from 'rxjs/internal/observable/interval';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.page.html',
  styleUrls: ['./change-password.page.scss'],
})

export class ChangePasswordPage implements OnInit {
  loginForm: FormGroup;

  constructor(public router: Router, private route: ActivatedRoute, public formBuilder: FormBuilder,
    public commonService: CommonService, public modalController: ModalController,
    public tools: Tools) {

    this.loginForm = this.formBuilder.group({
      old: ['', [Validators.required]],
      new: ['', [Validators.required]],
      confirm: ['', [Validators.required]]
    });

  }
  ngOnInit(): void {

  }

  onSubmit() {

    let old = this.loginForm.get('old').value;
    let newP = this.loginForm.get('new').value;
    let confirm = this.loginForm.get('confirm').value;
    if (old.trim().length < 1) {
      this.tools.openAlert('Please enter old password');
    } else if (newP.trim().length < 1) {
      this.tools.openAlert('Please enter new password');
    } else if (confirm.trim().length < 1) {
      this.tools.openAlert('Please enter confirm password');
    } else if (newP != confirm) {
      this.tools.openAlert('Your password and confirmation password do not match.');
    }
    else {
      this.tools.openLoader();
      this.commonService.changePassword(old, newP).subscribe(response => {
        this.tools.closeLoader();
        let res: any = response;
        this.loginForm.reset();
        this.tools.showToast(res.message, 2000);
        // this.router.navigateByUrl('/login', { replaceUrl: true });
      }, (error: Response) => {
        console.log(error);
        this.tools.closeLoader();
        let err: any = error;
        this.tools.openAlertToken(err.status, err.error.message);
      });
    }

  }

  goToLogin() {
    this.router.navigateByUrl('/');
  }

}
