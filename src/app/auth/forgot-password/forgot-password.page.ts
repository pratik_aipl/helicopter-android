import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})

export class ForgotPasswordPage implements OnInit {

  loginForm: FormGroup;

  constructor(public router: Router, public formBuilder: FormBuilder, public commonService: CommonService, public tools: Tools) {

    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });

  }
  ngOnInit() {

  }

  onSubmit() {

    let email = this.loginForm.get('email').value;
    if (email.trim().length < 1) {
      this.tools.openAlert('Please enter email');
    } else {
      this.tools.openLoader();
      this.commonService.forgotPassword(email).subscribe(response => {
        this.tools.closeLoader();
        let res: any = response;
        this.tools.showToast(res.message, 3000);
        this.router.navigateByUrl('/login', { replaceUrl: true });
      }, (error: Response) => {
        console.log(error);
        this.tools.closeLoader();
        let err: any = error;
        this.tools.openAlertToken(err.status, err.error.message);
      });
    }

  }

  goToLogin() {
    this.router.navigateByUrl('/');
  }

}
