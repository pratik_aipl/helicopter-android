import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';
import { ModalController } from '@ionic/angular';
import { SegmentModel } from 'src/app/shared/segment.model';
import { OneSignal } from '@ionic-native/onesignal/ngx';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {
  shouldDisable = true;
  pageType: any = 'email'
  img: any;
  loginForm: FormGroup;

  public segments: SegmentModel[] = [
    {
      key: 'email',
      label: 'Email ID'
    },
    {
      key: 'otp',
      label: 'Mobile No'
    }
  ];
  countyCode: any
  // public selectedSegment:string = this.segments[0].key;
  public selectedSegment: string = '';

  constructor(public router: Router, public formBuilder: FormBuilder, private route: ActivatedRoute,
    public oneSignal: OneSignal,
     public commonService: CommonService, public modalController: ModalController, public tools: Tools) {
    this.selectedSegment = this.route.snapshot.fragment != null ? this.route.snapshot.fragment : 'email';
    this.pageType = this.selectedSegment
    this.tools.callOneSignal();

    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      mno: [''],
    });
  }

  ngOnInit(): void {
 //   this.callOneSignal();
    this.route.fragment.subscribe(
      (fragment) => {
        this.setSegment(fragment);
      }
    );
  }
  selectContry(countyCode) {
    console.log('County Code ', countyCode);
    this.countyCode = countyCode;
  }
  onClickSegment(event: CustomEvent): void {
    if (event.detail && typeof event.detail.value === 'string') {
      const segment = event.detail.value;
      this.setSegment(segment);
      this.pageType = segment;
    }
  }

  setSegment(segment: string): void {
    if (typeof segment === 'string') {
      segment = segment.toLowerCase();
      const arrayHas = this.segments.some((candidate) => { return candidate.key === segment; });
      if (arrayHas) {
        this.selectedSegment = segment;
        this.router.navigate([], { fragment: segment }).then();
      }
    }
  }
  async goToHome() {
    // console.log('Social click')
    // const modal = await this.modalController.create({
    //   component: ModalComponent,
    //   cssClass: 'modal',
    //   componentProps: { value: this.selPass.PassAmount }
    // });
    // await modal.present();
  }
  goToRegister() {
    this.router.navigateByUrl('/register');
  }

  onSubmit() {

    if (this.pageType == 'email') {
      let email = this.loginForm.get('email').value;
      let password = this.loginForm.get('password').value;
      if (email.trim().length < 1) {
        this.tools.openAlert('Please enter email');
      } else if (password.trim().length < 1) {
        this.tools.openAlert('Please enter password');
      } else {
        this.tools.openLoader();
        this.commonService.login(email, password).subscribe(response => {
          this.tools.closeLoader();
          let res: any = response;
          this.loginForm.reset();
           localStorage.setItem('login_token', res.login_token);
          this.commonService.setEPass(res.data.pass[0]);
          this.commonService.setUserData(res.data.user, res.login_token);

          this.router.navigateByUrl('/home', { replaceUrl: true });
        }, (error: Response) => {
          console.log(error);
          this.tools.closeLoader();
          let err: any = error;
          this.tools.openAlertToken(err.status, err.error.message);
        });
      }
    } else {
      let mno = this.loginForm.get('mno').value;
      if (this.countyCode == undefined || this.countyCode == '') {
        this.tools.openAlert('Please select country code.');
      } else if (mno.trim().length < 1) {
        this.tools.openAlert('Please enter mobile no.');
      } else {
        this.tools.openLoader();
        this.commonService.sendOtp(this.countyCode, mno).subscribe(response => {
          this.tools.closeLoader();
          let res: any = response;
          this.loginForm.reset();
          // localStorage.setItem('login_token', res.login_token);
          // this.commonService.setEPass(res.data.pass[0]);
          // this.commonService.setUserData(res.data.user, res.login_token);
          this.router.navigateByUrl('verify-otp/' + this.countyCode + '/' + mno);
        }, (error: Response) => {
          console.log(error);
          this.tools.closeLoader();
          let err: any = error;
          this.tools.openAlertToken(err.status, err.error.message);
        });

      }

    }
  }

  goForgot() {
    this.router.navigateByUrl('forgot-password');
  }

 

}
