import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { AuthGuard } from './authguard.service';
// import { HTTP } from '@ionic-native/http/ngx';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { Tools } from '../tools';
import { OneSignal } from '@ionic-native/onesignal/ngx';
@Injectable({
  providedIn: 'root'
})
export class CommonService {

  deviceInfo;
  bacisAuth;
  options;
  httpOptions: any;

  constructor(public auth: AuthGuard, private http: HttpClient, public tool: Tools,public oneSignal: OneSignal) {
    this.deviceInfo = this.getDeviceInfo();
    this.bacisAuth = 'Basic ' + btoa(environment.username + ":" + environment.password);

    // this.http.setSSLCertMode('pinned');
    // this.http.setDataSerializer('utf8');

    // this.httpOptions = {
    //   'X-CI-HELICOPTER-API-KEY': environment.apikey,
    //   'Authorization': this.bacisAuth,
    //   'Access-Control-Allow-Headers': 'Accept, Content-Type, Content-Length, Accept-Encoding, Authorization, Origin'
    // };
    // this.setHeaderData();
    this.callOneSignal();
    this.setHeaderData();
  }

  dataURItoBlob(dataURI) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/jpeg' });
    return blob;
  }
  setHeaderData() {
    console.log('Basic auth ' + this.bacisAuth)

    console.log('getLoginToken ', this.getLoginToken());
    console.log('getUserId ', this.getUserId());

    if (this.getLoginToken() == undefined) {
      this.httpOptions = {
        headers: new HttpHeaders({
          'Access-Control-Allow-Origin': '*',
          'Authorization': this.bacisAuth,
          'X-CI-HELICOPTER-API-KEY': environment.apikey,
        })
      };
    } else {
      this.httpOptions = {
        headers: new HttpHeaders({
          // 'Content-Type': 'application/json',
          // 'Access-Control-Allow-Headers': "Access-Control-Allow-Headers,Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With",
          // 'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'Authorization': this.bacisAuth,
          'X-CI-HELICOPTER-API-KEY': environment.apikey,
          'User-Id': this.getUserId(),
          'X-CI-HELICOPTER-LOGIN-TOKEN': this.getLoginToken(),
        })
      };

    }
  }
  setHeaderDataNative() {
    if (this.auth.canActivate && this.getLoginToken() && this.getUserId()) {
      console.log('User Info ', this.getLoginToken());
      console.log('User Info ', this.getUserId());
      this.httpOptions = {
        'X-CI-HELICOPTER-API-KEY': environment.apikey,
        'Authorization': this.bacisAuth,
        'X-CI-HELICOPTER-LOGIN-TOKEN': this.getLoginToken(),
        'User-Id': this.getUserId(),
        'Access-Control-Allow-Headers': 'Accept, Content-Type, Content-Length, Accept-Encoding, Authorization, Origin'
      }
    }
  }

  login(email, password): any {

   // var dID;
    // console.log("pleayer-id login >>",typeof localStorage.getItem('PlearID'))
    // if (!localStorage.getItem('PlearID')) {
    //   dID = localStorage.getItem('PlearID') == '' ? "111111" : localStorage.getItem('PlearID');
    // } 

    var httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Authorization': this.bacisAuth,
        'X-CI-HELICOPTER-API-KEY': environment.apikey,
      })
    }
    console.log("pleayer dID login >>",localStorage.getItem('PlearID'))

    let postData = new FormData();
    // postData.append('file', imageFile);
    postData.append("EmailID", email);
    postData.append("Password", password);
    postData.append("PlayerID", localStorage.getItem('PlearID'));
    return this.http.post(environment.BaseUrl + 'login', postData, httpOptions);
  }
  forgotPassword(email): any {
    var httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Authorization': this.bacisAuth,
        'X-CI-HELICOPTER-API-KEY': environment.apikey,
      })
    }
    let postData = new FormData();
    // postData.append('file', imageFile);
    postData.append("email", email);
    return this.http.post(environment.BaseUrl + 'Register/forgot_password', postData, httpOptions);
  }
  changePassword(old, newp): any {
    var httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Authorization': this.bacisAuth,
        'X-CI-HELICOPTER-LOGIN-TOKEN': this.getLoginToken(),
        'X-CI-HELICOPTER-API-KEY': environment.apikey,
        'User-Id': this.getUserId(),
      })
    }
    let postData = new FormData();
    // postData.append('file', imageFile);
    postData.append("old_password", old);
    postData.append("new_password", newp);
    return this.http.post(environment.BaseUrl + 'Register/change_password', postData, httpOptions);
  }
  sendOtp(code, mno): any {

    // var dID;
    // if (!localStorage.getItem('PlearID')) {
    //   dID = localStorage.getItem('PlearID') == '' ? "111111" : localStorage.getItem('PlearID');
    // } else {
    //   dID = '111111'
    // }

    var httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Authorization': this.bacisAuth,
        'X-CI-HELICOPTER-API-KEY': environment.apikey,
      })
    }

    let postData = new FormData();
    // postData.append('file', imageFile);
    postData.append("CountryCode", code);
    postData.append("Mobile", mno);
    postData.append("PlayerID", localStorage.getItem('PlearID'));
    return this.http.post(environment.BaseUrl + 'login/send_otp', postData, httpOptions);
  }
  confirmopt(code, mno, otp): any {

    // var dID;
    // if (!localStorage.getItem('PlearID')) {
    //   dID = localStorage.getItem('PlearID') == '' ? "111111" : localStorage.getItem('PlearID');
    // } else {
    //   dID = '111111'
    // }

    var httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Authorization': this.bacisAuth,
        'X-CI-HELICOPTER-API-KEY': environment.apikey,
      })
    }

    let postData = new FormData();
    // postData.append('file', imageFile);
    postData.append("CountryCode", code);
    postData.append("MobileNo", mno);
    postData.append("OTP", otp);
    postData.append("PlayerID", localStorage.getItem('PlearID'));
    return this.http.post(environment.BaseUrl + 'login/confirmopt', postData, httpOptions);
  }
  register(user, data): any {
    data.append("PlayerID", localStorage.getItem('PlearID'));
    var httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Authorization': this.bacisAuth,
        'X-CI-HELICOPTER-API-KEY': environment.apikey,

      })
    }
    return this.http.post(environment.BaseUrl + 'register', data, httpOptions);
  }
  editProfile(data, file: any): any {
    var httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Authorization': this.bacisAuth,
        'X-CI-HELICOPTER-API-KEY': environment.apikey,
        'X-CI-HELICOPTER-LOGIN-TOKEN': this.getLoginToken(),
        'User-Id': this.getUserId(),

      })
    }
    return this.http.post(environment.BaseUrl + 'register/edit_profile', data, httpOptions);
  }
  countryList(): any {
    this.setHeaderData();
    let postData = new FormData();
    return this.http.post(environment.BaseUrl + 'Dashboard', postData, this.httpOptions);
  }
  payPass(data): any {
    var httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Authorization': this.bacisAuth,
        'X-CI-HELICOPTER-API-KEY': environment.apikey,
        'X-CI-HELICOPTER-LOGIN-TOKEN': this.getLoginToken(),
        'User-Id': this.getUserId(),
      })
    }
    return this.http.post(environment.BaseUrl + 'pay', data, httpOptions);
  }
  paypalVerifyMobile(data): any {
    var httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Authorization': this.bacisAuth,
        'X-CI-HELICOPTER-API-KEY': environment.apikey,
        'X-CI-HELICOPTER-LOGIN-TOKEN': this.getLoginToken(),
        'User-Id': this.getUserId(),

      })
    }

    return this.http.post(environment.BaseUrl + 'PaypalVerifyMobile', data, httpOptions);
  }

  PaypalVerifyLuggage(data): any {
    var httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Authorization': this.bacisAuth,
        'X-CI-HELICOPTER-API-KEY': environment.apikey,
        'X-CI-HELICOPTER-LOGIN-TOKEN': this.getLoginToken(),
        'User-Id': this.getUserId(),
      })
    }
    return this.http.post(environment.BaseUrl + 'PaypalVerifyLuggage', data, httpOptions);
  }

  bookNow(data): any {
    var httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Authorization': this.bacisAuth,
        'X-CI-HELICOPTER-API-KEY': environment.apikey,
        'X-CI-HELICOPTER-LOGIN-TOKEN': this.getLoginToken(),
        'User-Id': this.getUserId(),

      })
    }

    return this.http.post(environment.BaseUrl + 'BookNow', data, httpOptions);
  }
  bookedPass(): any {
    this.setHeaderData();
    return this.http.get(environment.BaseUrl + 'user-pass-list', this.httpOptions);
  }
  bookedbaglist(PassID): any {
    this.setHeaderData();
    return this.http.get(environment.BaseUrl + 'User_pass_list/pass_detail?PassID='+PassID, this.httpOptions);
  }
  Emergency(): any {
    return this.http.post(environment.BaseUrl + 'Emergency', {}, this.httpOptions);
  }
  passList(CountryID): any {
    return this.http.post(environment.BaseUrl + 'PassList', { 'CountryID': CountryID }, this.httpOptions);
  }

  SuppportSendMassage(data): any {
    var httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Authorization': this.bacisAuth,
        'X-CI-HELICOPTER-API-KEY': environment.apikey,
        'X-CI-HELICOPTER-LOGIN-TOKEN': this.getLoginToken(),
        'User-Id': this.getUserId(),
      })
    }
    return this.http.post(environment.BaseUrl + 'SupportChat', data, httpOptions);
  }
  GetGroupMemberList(data): any {
    var httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Authorization': this.bacisAuth,
        'X-CI-HELICOPTER-API-KEY': environment.apikey,
        'X-CI-HELICOPTER-LOGIN-TOKEN': this.getLoginToken(),
        'User-Id': this.getUserId(),
      })
    }
    return this.http.post(environment.BaseUrl + 'Groupchat/group_member_list', data, httpOptions);
  }
  DeleteMember(data): any {
    var httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Authorization': this.bacisAuth,
        'X-CI-HELICOPTER-API-KEY': environment.apikey,
        'X-CI-HELICOPTER-LOGIN-TOKEN': this.getLoginToken(),
        'User-Id': this.getUserId(),
      })
    }
    return this.http.post(environment.BaseUrl + 'Groupchat/left_group', data, httpOptions);
  }
  GetBagPass(): any {
    this.setHeaderData();
    return this.http.get(environment.BaseUrl + 'luggage-bag-list', this.httpOptions);
  }
  getsupportchat(): any {
    this.setHeaderData();
    return this.http.get(environment.BaseUrl + 'SupportChat/chat', this.httpOptions);
  }
  
  getuserchatlist(): any {
    this.setHeaderData();
    console.log('User list');
    return this.http.get(environment.BaseUrl + 'UserChatList', this.httpOptions);
  }
  getgrouplist(): any {
    this.setHeaderData();
    return this.http.get(environment.BaseUrl + 'Groupchat', this.httpOptions);
  }

  pendingMessages(data): any {
    console.log(data);
    return this.http.post(environment.chatUrl+'/chat/PendingMessages', data, this.httpOptions);
  }
  groupPendingMessages(data): any {
    console.log(data);
    return this.http.post(environment.chatUrl+'/group_chat/PendingMessages', data, this.httpOptions);
  }
  CreateNewGroup(data): any {
    var httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Authorization': this.bacisAuth,
        'X-CI-HELICOPTER-API-KEY': environment.apikey,
        'X-CI-HELICOPTER-LOGIN-TOKEN': this.getLoginToken(),
        'User-Id': this.getUserId(),
      })
    }
    return this.http.post(environment.BaseUrl + 'Groupchat/create_group', data, httpOptions);
  }
  
  AddMemberToGroup(data): any {
    var httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Authorization': this.bacisAuth,
        'X-CI-HELICOPTER-API-KEY': environment.apikey,
        'X-CI-HELICOPTER-LOGIN-TOKEN': this.getLoginToken(),
        'User-Id': this.getUserId(),
      })
    }
    return this.http.post(environment.BaseUrl + 'Groupchat/add_group_member', data, httpOptions);
  }
  
  makePayment(data): any {
    var httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + environment.secretkey,
      })
    }
    return this.http.post(environment.stripeChargeUrl, data, httpOptions);
  }
  getRideHistory(): any {
    this.setHeaderData();
    return this.http.get(environment.BaseUrl + 'MyRideList', this.httpOptions);
  }


  getUserInfo(): any {
    this.setHeaderData();
    let postData = new FormData();
    return this.http.post(environment.BaseUrl + 'UserInfo', postData, this.httpOptions);
  }



  rideRatingPost(data): any {
    this.setHeaderData();
    return this.http.post(environment.BaseUrl + 'MyRideList/ratting_ride', data, this.httpOptions);
  }
  getCurrentLatLng() {
    navigator.geolocation.getCurrentPosition((res) => {
      let currentLatLng: any = {};
      currentLatLng.latitude = res.coords.latitude;
      currentLatLng.longitude = res.coords.longitude;
      return currentLatLng;
    });
  }

  // GET & SET USER DATA
  setSelPass(bookedPass) {
    console.log(bookedPass)
    window.localStorage.setItem('bookedPass', JSON.stringify(bookedPass));
  }
  getEPass() {
    if (window.localStorage['ePass']) {
      return JSON.parse(window.localStorage['ePass']);
    }
    return;
  }
  // GET & SET USER DATA
  setEPass(ePass) {
    console.log(ePass)
    window.localStorage.setItem('ePass', JSON.stringify(ePass));
  }
  getSelPass() {
    if (window.localStorage['bookedPass']) {
      return JSON.parse(window.localStorage['bookedPass']);
    }
    return;
  }
  setSelRide(selectedRide) {
    console.log(selectedRide)
    window.localStorage.setItem('seleRide', JSON.stringify(selectedRide));
  }
  getSelRide() {
    if (window.localStorage['seleRide']) {
      return JSON.parse(window.localStorage['seleRide']);
    }
    return;
  }
  setUserData(userData, login_token) {
    console.log(userData)
    window.localStorage.setItem('user_data', JSON.stringify(userData));
    if (login_token != '')
      window.localStorage.setItem('login_token', login_token);
    window.localStorage.setItem('user_id', userData.id);
  }
  setSelectedCountry(item) {
    window.localStorage.setItem('sel_route', JSON.stringify(item));
  }

  getSelectedCountry() {
    if (window.localStorage['sel_route']) {
      return JSON.parse(window.localStorage['sel_route']);
    }
    return;
  }
  setSelectedRoute(item) {
    window.localStorage.setItem('sel_route_sub', JSON.stringify(item));
  }
  getSelectedRoute() {
    if (window.localStorage['sel_route_sub']) {
      return JSON.parse(window.localStorage['sel_route_sub']);
    }
    return;
  }
  setSelectedPass(item) {
    if (item != null)
      window.localStorage.setItem('selPass', JSON.stringify(item));
    else
      window.localStorage.setItem('selPass', '');
  }
  getSelectedPass() {
    if (window.localStorage['selPass']) {
      return JSON.parse(window.localStorage['selPass']);
    }
    return;
  }
  getUserData() {
    if (window.localStorage['user_data']) {
      return JSON.parse(window.localStorage['user_data']);
    }
    return;
  }
  getUserId() {
    if (window.localStorage['user_id']) {
      return window.localStorage['user_id'];
    }
    return;
  }

  getLoginToken() {
    if (window.localStorage['login_token']) {
      return window.localStorage['login_token'];
    }
    return;
  }


  // GET & SET DEVICE INFO
  setDeviceInfo(deviceInfo) {
    window.localStorage.setItem('deviceInfo', JSON.stringify(deviceInfo));
  }

  getDeviceInfo() {
    if (window.localStorage['deviceInfo']) {
      return JSON.parse(window.localStorage['deviceInfo']);
    }
    return;
  }
  callOneSignal() {
    this.oneSignal.startInit('9b0e84fb-5e5c-42dc-8582-84ef7c9e4c52','546015657170');

    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
    
    this.oneSignal.handleNotificationReceived().subscribe(() => {
      // do something when notification is received
     });
     
     this.oneSignal.handleNotificationOpened().subscribe(() => {
       // do something when a notification is opened
     });
  
    this.oneSignal.endInit();
    this.oneSignal.getIds().then((id) => {
      console.log('userId login ==> ',id.userId);
      console.log('pushToken ==> ',id.pushToken);
      localStorage.setItem('PlearID',id.userId);
    });
  }
}
