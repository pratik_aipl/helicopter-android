import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
// import { CommonModule } from '@angular/common';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AuthGuard } from './authguard.service';
import { Tools } from '../tools';
import { DatePipe } from '@angular/common';
import { CountryPickerComponent } from '../widgets/country/country-picker.component';



@NgModule({
  declarations: [CountryPickerComponent],
  imports: [
    CommonModule,
    IonicModule,FormsModule,
    ReactiveFormsModule
    ],
  exports: [CommonModule, IonicModule, CountryPickerComponent,  ReactiveFormsModule],
  providers: [AuthGuard, Tools, DatePipe],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class SharedModule { }
