import { Component, OnInit } from '@angular/core';
import { MenuController, Events, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { CommonService } from '../shared/common.service';
import { Tools } from '../tools';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage implements OnInit {
  routeList = [];
  user: any = {};
  constructor(private menu: MenuController, public alertController: AlertController,
    public commonService: CommonService,
    public tools: Tools, public events: Events,
    private router: Router) {
    this.user = this.commonService.getUserData();
    events.subscribe('profileUpdate', (item) => {
      this.user = item;
      console.log('Event call')
    });
  }

  ngOnInit() {
  }
  // ionViewWillEnter(){
  //   console.log('ionViewWillEnter --1 ');
  // }
  ionViewDidEnter() {
    console.log('ionViewDidEnter --2 ');
    this.getUserData();
  }
  EmergencyPopup() {
    if (this.user.IsEmergency != 0) {
      this.presentEmergency('Please confirm this is a "*LIFE or DEATH * emergency', 'CONFIRM', 'CANCEL', true);
    } else {
      this.presentEmergency('In order to access the Emergency pick-up you must obtain the Great Scott Pass.', 'PURCHASE', 'CANCEL', false);
    }

  }

 

  Emergency() {
    this.tools.openLoader();
    this.commonService.Emergency().subscribe(response => {
      this.tools.closeLoader();
      let res: any = response;
      console.log('callEmergency --> ', res.message)
      console.log('callEmergency --> ', res.data)
      this.tools.openAlert(res.message);
      //this.passList = (JSON.parse(data.data).data);
    }, (error: Response) => {
      this.tools.closeLoader();
      console.log(error);
      let err: any = error;
      this.tools.openAlertToken(err.status, err.error.message);
    });
  }


  getUserData() {
    //this.tools.openLoader();
    this.commonService.getUserInfo().subscribe(response => {
      this.tools.closeLoader();
      let res: any = response;
      this.commonService.setEPass(res.data.pass[0]);
      this.commonService.setUserData(res.data.user, this.commonService.getLoginToken());

      this.getCountryList();
      this.user = res.data.user;
      if (this.user.CountryCode == '') {
        this.tools.openUpdateProfile();
      }
    }, (error: Response) => {
      this.tools.closeLoader();
      console.log(error);
      let err: any = error;
      this.tools.openAlertToken(err.status, err.error.message);
    });
  }
  getCountryList() {
    this.tools.openLoader();
    this.commonService.countryList().subscribe(data => {
      this.tools.closeLoader();

      let res: any = data;

      console.log(' Response ', res);
      this.commonService.setSelectedCountry('')
      this.routeList = res.data;

    }, (error: Response) => {
      this.tools.closeLoader();

      console.log(error);

      let err: any = error;
      this.tools.openAlertToken(err.status, err.error.message);
    });
  }

  openFirst() {
    this.menu.enable(true, 'first');  // replace with MenuA for your case
    this.menu.open('first');
  }
  myProfile() {
    this.menu.close();
    this.router.navigateByUrl('/myprofile');
  }
  changePassword() {
    this.menu.close();
    this.router.navigateByUrl('/change-password');
  }
  overService() {
    this.menu.close();
    this.router.navigateByUrl('/our-services');
  }
  myRideHistory() {
    this.menu.close();
    this.router.navigateByUrl('/myrides/home');
  }
  myBookings() {
    this.menu.close();
    this.router.navigateByUrl('/mybookings');
  }
  bookPass() {
    this.menu.close();
    this.commonService.setSelectedCountry('')
    this.commonService.setSelectedPass(null);
    this.router.navigateByUrl('/book-pass');

  }

  ChatClick() {
    this.menu.close();
    //this.router.navigateByUrl('/chatuserlist');
    this.router.navigateByUrl('/adminsupportchat');

  }
  UserChatClick() {
    this.menu.close();
    this.router.navigateByUrl('/chatuserlist');

  }
  settingClick() {
    this.menu.close();
    this.router.navigateByUrl('/setting');
  }

  logout() {
    this.menu.close();
    localStorage.clear();
    this.router.navigateByUrl('/login');
  }
  routeClick(item) {
    this.commonService.setSelectedCountry(item)
    this.router.navigate(['/route-list'], { queryParams: { q: item } });

  }

  async presentEmergency(message, btnYes, btnNo, isPass) {
    const alert = await this.alertController.create({
      message: message,
      buttons: [
        {
          text: btnNo ? btnNo : 'Cancel',
          role: 'cancel',
          handler: () => {

          }
        },
        {
          text: btnYes ? btnYes : 'Yes',
          handler: () => {
            if (isPass)
              this.Emergency();
            else {
              this.user = this.commonService.getUserData();
              this.commonService.setSelectedPass(this.commonService.getEPass());
              this.router.navigateByUrl('/pass-details/purchase');
            }
          }
        }
      ], backdropDismiss: true
    });
    return await alert.present();
  }
}
