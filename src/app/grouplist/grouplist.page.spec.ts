import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrouplistPage } from './grouplist.page';

describe('GrouplistPage', () => {
  let component: GrouplistPage;
  let fixture: ComponentFixture<GrouplistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrouplistPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrouplistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
