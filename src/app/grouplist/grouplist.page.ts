import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from '../shared/common.service';
import { Tools } from '../tools';

@Component({
  selector: 'app-grouplist',
  templateUrl: './grouplist.page.html',
  styleUrls: ['./grouplist.page.scss'],
})
export class GrouplistPage implements OnInit {
  userList=[];
  newuserList=[];
  constructor( public commonService: CommonService,private tools: Tools ,public router: Router) { 
  }
  ionViewDidEnter() {
    this.GetGroupList();
    }
  GetGroupList() {
   
    this.tools.openLoader();
    console.log('callgetPendingMessage ');

    this.commonService.getgrouplist().subscribe(data => {
      this.tools.closeLoader();
      let res: any = data;
      this.userList = res.data
      this.newuserList = res.data
      console.log('Groupchat => Groupchat ', res);
      // this.bookingList = res.data;
    }, (error: Response) => {
      console.log('bookedPass ==> ',error)
      console.log(error);
      this.tools.closeLoader();
      let err:any = error;
      this.tools.openAlertToken(err.status, err.error.message);      
    });
  
  }
  ngOnInit() {
    
  }
  Groupchatdata(item){
    console.log(" select group data >>",item);
    this.router.navigateByUrl('/groupchat/'+item.GroupID+'/'+item.GroupTitle+'/'+item.isAdmin);
  }
  
  creategroup(){
    this.router.navigateByUrl('/creategroup');
  }


  getItems(ev: any) {
    // Reset items back to all of the items
   // this.initializeItems();
    this.userList=this.newuserList;
    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() !== '') {
       // this.isItemAvailable = true;
        this.userList = this.userList.filter((chat) => {
            return (chat.GroupTitle.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
    } else {
       // this.isItemAvailable = false;
    }
}
}

