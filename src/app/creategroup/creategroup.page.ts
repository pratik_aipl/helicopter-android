import { ToastController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from '../shared/common.service';
import { Tools } from '../tools';

@Component({
  selector: 'app-creategroup',
  templateUrl: './creategroup.page.html',
  styleUrls: ['./creategroup.page.scss'],
})
export class CreateGroupPage implements OnInit {
  userList=[];
  groupname= '';
  constructor( public toastCtrl: ToastController, public commonService: CommonService,private tools: Tools ,public router: Router) { 
   
  }

  GetUserList() {
   
    this.tools.openLoader();
    console.log('callgetPendingMessage ');

    this.commonService.getuserchatlist().subscribe(data => {
      this.tools.closeLoader();
      let res: any = data;

      for (let i = 0; i < res.chat_list.length; i++) {
        const element = res.chat_list[i];
        element.IsSelected = false;
        this.userList.push(element)         
      }
      console.log('Data => userList ', res);
    }, (error: Response) => {
      console.log('bookedPass ==> ',error)
      console.log(error);
      this.tools.closeLoader();
      let err:any = error;
      this.tools.openAlertToken(err.status, err.error.message);      
    });
  
  }

  ngOnInit() {
    this.GetUserList();
  }

  creategroup(){
    
    const storeId:any = [];
    for (let i = 0; i < this.userList.length; i++) {
      if (this.userList[i].IsSelected) {
        storeId.push(this.userList[i].id);
      }
    }
    console.log("ID >>> ",storeId.join(','))


    if(this.groupname==''){
      this.showToast('Enter Group Name');

    }else if(storeId.length==0){
      this.showToast('Select Any User');
    }else{
      this.CreateNewGroup(storeId);
    }

  }

  CreateNewGroup(storeId) {
    this.tools.openLoader();
    let  postData = new FormData();
    postData.append('GroupTitle',this.groupname);
    postData.append('MemberID',storeId.join(','));
   
    this.commonService.CreateNewGroup(postData).subscribe(data => {
      this.tools.closeLoader();
      // console.log(JSON.parse(data.data).data);
      let res: any = data;
      console.log('greoup data >>> ',res);

      if(res.status){
         // this.router.navigateByUrl('/grouplist', { replaceUrl: true });
          this.router.navigateByUrl('/grouplist');
      }else{
        this.showToast(res.message);
      }
    }, (error: Response) =>  {
      this.tools.closeLoader();
      let err:any = error;
      console.log('Booking error --> ',err);
      this.tools.openAlertToken(err.status, err.error.message);
 
    });
  }
  changeChacked(event, i) {
    console.log("event >>",event)
    this.userList[i].IsSelected = event.detail.checked; //result.data.userstore;
  }
  async showToast(message: string) {
    let toast = await this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'middle'
    });

    toast.present();
  }
}
