import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { Camera } from '@ionic-native/camera/ngx';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthGuard } from './shared/authguard.service';
import { HttpClientModule } from '@angular/common/http';
import { Tools } from './tools';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
//const config: SocketIoConfig = { url: 'http://localhost:3001', options: {} };

const normalchat: SocketIoConfig = { url: 'http://164.52.213.73:3600', options: {} };
//const groupchat: SocketIoConfig = { url: 'http://164.52.213.73:7005', options: {} };

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, 
    // BaiduMapModule.forRoot({ ak: 'XfddZtHMxcBcRXL3w5BnXndiCwl7ptyE' }) //sercer
  //  ,
    IonicModule.forRoot({
      mode: 'md',
      scrollAssist: false
    }),HttpClientModule,
    AppRoutingModule, SocketIoModule.forRoot(normalchat),
  ],
  providers: [
    StatusBar,
    SplashScreen, OneSignal,
    AuthGuard,
    Tools,
    Camera,
    WebView,
    ImagePicker,InAppBrowser,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
