import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Events, ToastController } from '@ionic/angular';
import { Socket } from 'ngx-socket-io';
import { Tools } from 'src/app/tools';
import { CommonService } from '../shared/common.service';
@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {
 

  message = '';
  messages = [];
  currentUser = '';
  receiverID: any;
  name: any;
  typing = false;
  task: any;
  user: any;
  from: any;
  page: any;
  topPoint=50;
  UserPic:any;
  @ViewChild('content') private content: any;
  @ViewChild('inMsg') yourElement: ElementRef;

  constructor(private tools: Tools, private socket: Socket,public events: Events, public commonService: CommonService, private activatedRoute: ActivatedRoute, private toastCtrl: ToastController) {
    // this.task = this.commonService.getSelectedTask();
    // this.page = this.activatedRoute.snapshot.paramMap.get('page');
    // this.from = this.activatedRoute.snapshot.paramMap.get('from');
    // console.log('Chat Screen ', this.from);
    this.receiverID = this.activatedRoute.snapshot.paramMap.get('type');
    this.name = this.activatedRoute.snapshot.paramMap.get('name');
    this.user = this.commonService.getUserData();
   // this.UserPic = this.activatedRoute.snapshot.paramMap.get('userpic');
    console.log('params userpic =>', this.UserPic);
    this.UserPic=window.localStorage['UserPic'];
    

     console.log("UserPic  >>"+this.UserPic);
    // console.log("from >>"+this.from);
    // console.log("receiverID >>"+this.receiverID);
    // console.log("getUserId >>"+this.commonService.getUserId());
    // console.log("this.user >>"+this.user);
  }

  
  ngOnInit() {
    this.callgetPendingMessage(true);
    this.socket.connect();
    this.currentUser = this.user.name;
    const data = {
      senderID: this.commonService.getUserId(),
      receiverID: this.receiverID,
      type: true
    }
    // if(this.page == 'list')
    this.socket.emit('set-name', data);

    this.socket.on('connection', (socket) => { 
      console.log('Connection ',socket)
    });
    
    this.socket.fromEvent('typing').subscribe(typing => {
      this.typing = true;
    });
    this.socket.fromEvent('stopTyping').subscribe(stopTyping => {
      console.log('Stop Typing --> ');
      this.typing = false;
    });
    this.socket.fromEvent('chat message').subscribe(message => {
      console.log('chat msg -->',message);
      const msg = {
        SenderId: this.receiverID,
        Message: message,
        UpdatedAt: new Date(),
      }
      this.messages.push(msg);
      this.typing = false;
      if(this.topPoint>60){
        this.scrollToLatestMessage();
      }
      // this.grid.scrollToBottom();
    });


  }

  scrollFunction(ev) {
   this.topPoint=ev.detail.scrollTop
    // console.log('scroll event scrollTop', ev.detail.scrollTop);
    // console.log('scroll event detail', ev.detail);
   }

  callgetPendingMessage(isFill) {
    const data = {
      senderID: this.commonService.getUserId(),
      receiverID:this.receiverID,
     // TaskId: this.task.TaskID,
    }
    this.tools.openLoader();
    console.log('callgetPendingMessage ');

    this.commonService.pendingMessages(data).subscribe(data => {
      this.tools.closeLoader();
      let res: any = data;
       this.messages = res.messages;
      console.log('Data => userList ',  this.messages);
      // this.bookingList = res.data;
    }, (error: Response) => {
      console.log('bookedPass ==> ',error)
      console.log(error);
      this.tools.closeLoader();
      let err:any = error;
      this.tools.openAlertToken(err.status, err.error.message);      
    });

  }

  onChangeTime(data): void {
    console.log("onChangeTime to time: " + this.message + ". Event data: " + data);
    this.socket.emit("typing", { msg: this.message, senderID: this.commonService.getUserId(), receiverID: this.receiverID });
    setTimeout(() => {
      this.socket.emit("stopTyping", { msg: this.message, senderID: this.commonService.getUserId(), receiverID: this.receiverID });
    }, 1000);
  }

  sendMessage() {
    this.socket.emit("chat message", { msg: this.message, senderID: this.commonService.getUserId(), receiverID: this.receiverID });
    this.socket.emit("msgCount", { senderID: this.commonService.getUserId(), receiverID: this.receiverID });
    // this.events.publish('msgCount'); 
    const msg = {
      SenderId: this.commonService.getUserId(),
      Message: this.message,
      UpdatedAt: new Date(),
    }
    this.messages.push(msg);
    this.message = '';
    this.scrollToLatestMessage();
  }

  ionViewWillLeave() {
    console.log('ionViewWillLeave ');
    this.events.publish('msgCount'); 
   // this.callgetPendingMessage(false);
  //  this.socket.emit("msgCount", { senderID: this.receiverID, receiverID: this.commonService.getUserId(), taskID: this.task.TaskID });
//    this.socket.disconnect();
  }

  scrollToLatestMessage(): void {
    setTimeout(() => {
      this.content.scrollToBottom(1500);
    }, 300);
  }


  async showToast(msg) {
    let toast = await this.toastCtrl.create({
      message: msg,
      position: 'top',
      duration: 2000
    });
    toast.present();
  }
}
