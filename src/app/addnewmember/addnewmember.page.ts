import { ToastController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from '../shared/common.service';
import { Tools } from '../tools';

@Component({
  selector: 'app-addnewmember',
  templateUrl: './addnewmember.page.html',
  styleUrls: ['./addnewmember.page.scss'],
})
export class AddNewMemberPage implements OnInit {
  userList=[];
  groupname= '';
  groupID: any;


  constructor( public toastCtrl: ToastController,private activatedRoute: ActivatedRoute, public commonService: CommonService,private tools: Tools ,public router: Router) { 
    this.groupID = this.activatedRoute.snapshot.paramMap.get('id');
    console.log("this.groupID  >>"+this.groupID);

  }

  GetUserList() {
   
    this.tools.openLoader();
    console.log('callgetPendingMessage ');

    this.commonService.getuserchatlist().subscribe(data => {
      this.tools.closeLoader();
      let res: any = data;

      for (let i = 0; i < res.chat_list.length; i++) {
        const element = res.chat_list[i];
        element.IsSelected = false;
        this.userList.push(element)         
      }
      console.log('Data => userList ', res);
    }, (error: Response) => {
      console.log('bookedPass ==> ',error)
      console.log(error);
      this.tools.closeLoader();
      let err:any = error;
      this.tools.openAlertToken(err.status, err.error.message);      
    });
  
  }
  ionViewDidEnter() {
    this.GetUserList();
    }
  ngOnInit() {
  }

  addingtogroup(){
    
    const storeId:any = [];
    for (let i = 0; i < this.userList.length; i++) {
      if (this.userList[i].IsSelected) {
        storeId.push(this.userList[i].id);
      }
    }
    console.log("ID >>> ",storeId.join(','))


   if(storeId.length==0){
      this.showToast('Select Any Member');
    }else{
      this.AddMemberToGroup(storeId);
    }

  }

  AddMemberToGroup(storeId) {
    this.tools.openLoader();
    let  postData = new FormData();
    postData.append('GroupID',this.groupID);
    postData.append('MemberID',storeId.join(','));
   
    this.commonService.AddMemberToGroup(postData).subscribe(data => {
      this.tools.closeLoader();
      let res: any = data;

      if(res.status){
       this.userList=[];
          this.router.navigateByUrl('/grouplist');
      }else{
        this.showToast(res.message);
      }
    }, (error: Response) =>  {
      this.tools.closeLoader();
      let err:any = error;
      console.log('Booking error --> ',err);
      this.tools.openAlertToken(err.status, err.error.message);
 
    });
  }
  changeChacked(event, i) {
    console.log("event >>",event)
    this.userList[i].IsSelected = event.detail.checked; //result.data.userstore;
  }
  async showToast(message: string) {
    let toast = await this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'middle'
    });

    toast.present();
  }
}
