import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from '../shared/common.service';
import { Tools } from '../tools';

@Component({
  selector: 'app-chatuserlist',
  templateUrl: './chatuserlist.page.html',
  styleUrls: ['./chatuserlist.page.scss'],
})
export class ChatuserlistPage implements OnInit {
  userList=[];
  newuserList=[];
  constructor( public commonService: CommonService,private tools: Tools ,public router: Router) { 
    // for (let i = 0; i < 5; i++) {
    //   this.chatList[i]="sample ";
      
    // }
  }

  GetUserChatList() {
   
    this.tools.openLoader();
    console.log('callgetPendingMessage ');

    this.commonService.getuserchatlist().subscribe(data => {
      this.tools.closeLoader();
      let res: any = data;
      this.userList = res.chat_list
      this.newuserList = res.chat_list
      console.log('Data => userList ', res);
      // this.bookingList = res.data;
    }, (error: Response) => {
      console.log('bookedPass ==> ',error)
      console.log(error);
      this.tools.closeLoader();
      let err:any = error;
      this.tools.openAlertToken(err.status, err.error.message);      
    });
  
  }

  ngOnInit() {
    this.GetUserChatList();
  }
  userchatdata(item){
    console.log("this.UserPic list >>",item);

    this.router.navigateByUrl('/chat/'+item.id+'/'+(item.firstname +' '+item.lastname ) +'/'+item.UserPic);
    window.localStorage.setItem('UserPic', item.UserPic);
    // this.router.navigateByUrl('/chat');
  }
  groupuserlist(){
    //this.router.navigateByUrl('/chat'+item);
   this.router.navigateByUrl('/grouplist');
  }


  getItems(ev: any) {
    // Reset items back to all of the items
   // this.initializeItems();
    this.userList=this.newuserList;
    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() !== '') {
       // this.isItemAvailable = true;
        this.userList = this.userList.filter((chat) => {
            return (chat.firstname.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
    } else {
       // this.isItemAvailable = false;
    }
}
}
