import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatuserlistPage } from './chatuserlist.page';

describe('ChatuserlistPage', () => {
  let component: ChatuserlistPage;
  let fixture: ComponentFixture<ChatuserlistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatuserlistPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatuserlistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
