import { GroupMemberListPage } from './groupmemberlist.page';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';


describe('ChatuserlistPage', () => {
  let component: GroupMemberListPage;
  let fixture: ComponentFixture<GroupMemberListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupMemberListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupMemberListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
