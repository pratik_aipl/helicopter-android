import { AlertController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from '../shared/common.service';
import { Tools } from '../tools';

@Component({
  selector: 'app-groupmemberlist',
  templateUrl: './groupmemberlist.page.html',
  styleUrls: ['./groupmemberlist.page.scss'],
})
export class GroupMemberListPage implements OnInit {
  memberList=[]
  isAdmin: any;
  groupID: any;
  name: any;
  user: any;

  constructor( public commonService: CommonService,public alertController: AlertController,private activatedRoute: ActivatedRoute,private tools: Tools ,public router: Router) { 
    this.groupID = this.activatedRoute.snapshot.paramMap.get('id');
    this.isAdmin = this.activatedRoute.snapshot.paramMap.get('isAdmin');
    this.name = this.activatedRoute.snapshot.paramMap.get('name');
    this.user = this.commonService.getUserData();

    console.log("this.groupID  >>"+this.groupID);
    console.log("this.name  >>"+this.name);
    console.log("this.isAdmin  >>"+this.isAdmin);
  }
  ngOnInit() {
    this.GetMemberList();
  }

  GetMemberList() {
    this.tools.openLoader();
    let  postData = new FormData();
    postData.append('GroupID',this.groupID);
    this.commonService.GetGroupMemberList(postData).subscribe(data => {
      this.tools.closeLoader();
      let res: any = data;
      this.memberList = res.data
      console.log('Data => userList ', res);
    }, (error: Response) => {
      console.log('bookedPass ==> ',error)
      console.log(error);
      this.tools.closeLoader();
      let err:any = error;
      this.tools.openAlertToken(err.status, err.error.message);      
    });
  
  }


  

  deleteMember(MemberID,Type) {
    this.tools.openLoader();
    let  postData = new FormData();
    postData.append('GroupID',this.groupID);
    postData.append('MemberID',MemberID);
    this.commonService.DeleteMember(postData).subscribe(data => {
      this.tools.closeLoader();
      let res: any = data;

      if(Type=="exit"){
        this.router.navigateByUrl('/grouplist');
      }else{
        this.GetMemberList();
      }
    }, (error: Response) => {
      console.log('bookedPass ==> ',error)
      console.log(error);
      this.tools.closeLoader();
      let err:any = error;
      this.tools.openAlertToken(err.status, err.error.message);      
    });
  
  }


  exitGroup(){
   this.presentConfirm('Do you want to exit this Group?', '', '',this.user.id,'exit');     
  }
 
  addmember(){
    this.router.navigateByUrl('/addnewmember/'+this.groupID);
  }


  deleteuser(MemberID){
    this.presentConfirm('Do you want to remove this member?', '','',MemberID,'delete');      

  }

  async presentConfirm(message, btnYes, btnNo,MemberID,Type) {
    const alert = await this.alertController.create({
        message: message ? message : 'Do you want to this?',
        buttons: [
            {
                text: btnNo ? btnNo : 'NO',
                role: 'cancel',
                handler: () => {
                    
                }
            },
            {
                text: btnYes ? btnYes : 'Yes',
                handler: () => {
                  this.deleteMember(MemberID,Type);
                }
            }
        ], backdropDismiss: false
    });
    return await alert.present();
}
}
